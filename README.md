# greentech.training (cisp-web)

The CISP's main interface with the world, i.e. the public (or as some put it, the marketing ...) pages.


## Developement

> This is a `next.js` app created with `yarn create next-app`; See https://nextjs.org/docs


To run the local developement server

1. `yarn` to install the dependencies
1. `yarn dev` to launch the local server
1. open the local server in the browser, and navigate to `/projects/greentech.training/`


### Admin page (netlify-cms)

To edit the content of the site through a CMS, visit the admin page at:

- Online, at http://publiservices.gitlab.io/projects/greentech.training/admin/index.html
- In localhost at http://localhost:3001/projects/greentech.training/admin/index.html

> It is always possible to edit the content of the site in the markdown files directly, and in the code.

#### Local netlify-cms server

It is possible to run a local netlify-cms server/backend:

1. run the next.js app normally
1. run `npx netlify-cms-proxy-server` in the app folder's root
1. in `./public/admin/options.js`, `config.local_backend: true`
1. visit the local admin page normally

Docs: https://www.netlifycms.org/docs/beta-features/#working-with-a-local-git-repository

All changes will be made to local files (and not to the git host), directly in the project's folder.


### i18n, multilanguage

The website is setup to support multiple languages. Though, currently, it is set to accept only German (`de`), the CMS and the site can work with i18n.

See the commented out `next.config.js.i18n` object, and [why in the gitlab issue](https://gitlab.com/greentech-cisp/cisp-web/-/issues/2).

> The cms is currently disabling i18n, because there is no support for
> only 1 language, and having both language makes error when editing
> content where the second language file does not exist in the
> repository.
> To activate i18n in cms, see comment in `/admin/options.js`
