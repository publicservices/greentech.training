import styles from '@/styles/components/services/card.module.css'

export default function Card ({
	active,
	service,
	onClick = () => {},
}) {
	const {
		slug,
		title,
		steps,
	} = service.frontmatter

	const handleClick = () => {
		!active ? onClick(service) : onClick()
	}

	return (
		<article className={active ? styles.rootActive : styles.root} onClick={handleClick}>
			<header className={styles.header}>
				<h2 className={styles.title}>
					{title}
				</h2>
			</header>
			<main className={styles.main}>
				{steps ? (
					<ol className={styles.steps}>
						{steps.map((step, index) => (
							<li className={styles.step} key={index}>
								{step.name}
							</li>
						))}
					</ol>
				) : null}
			</main>
		</article>
	)
}
