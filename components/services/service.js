import pageStyles from '@/styles/pages/course.module.css'

import Link from 'next/link'

export default function Course ({course}) {
	const {slug, frontmatter} = course
	const {title, content, description_short, duration} = frontmatter
	return (
		<article className={pageStyles.root}>
			<header className={pageStyles.header}>
				<h1 className={pageStyles.title}>
					<Link href={`/courses/${slug}`}>{title}</Link>
				</h1>
			</header>
			<aside>
				{description_short}
			</aside>
			<main>
				{course.content}
			</main>
		</article>
	)
}
