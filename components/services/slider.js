import { useState, useEffect, useRef } from "react";

import List from "./list";
import nextArrow from "../../public/media/icons/next-arrow.png";
import cardStyles from "@/styles/components/services/card.module.css";
import Image from "next/image";

export default function Slider({
	active = false,
	services,
	defaultTitle,
	defaultContent,
	onClick = () => {},
}) {
	const [activeService, setActiveService] = useState(
		active ? services[0] : null
	);
	const ref = useRef(null);
	const handleCarClick = (service = null) => {
		setActiveService(service);
		onClick(service);
	};
	const handleNext = () => {
		const nextServiceIndex =
			services.indexOf(activeService) + (1 % services.length);
		setActiveService(services[nextServiceIndex]);
	};
	const handlePrevious = () => {
		setActiveService(null);
	};

	// automatically reset the slider when the user taps out of the it
	useEffect(() => {
		function handleClickOutside(event) {
			if (ref.current && !ref.current.contains(event.target)) {
				setActiveService(null);
			}
		}
		// Bind the event listener
		window.addEventListener("mousedown", handleClickOutside);
		return () => {
			// Unbind the event listener on clean up
			window.removeEventListener("mousedown", handleClickOutside);
		};
	}, [ref, services]);
	return (
		<article className={cardStyles.slider}>
			<main className={cardStyles.sliderMain}>
				<header className={cardStyles.sliderTitle}>
					<h2>
						{activeService ? activeService.frontmatter.title : defaultTitle}
					</h2>
				</header>
				<aside className={cardStyles.sliderContent}>
					{activeService ? activeService.content : defaultContent}
				</aside>
			</main>

			<aside ref={ref} className={cardStyles.sliderSlides}>
				<List
					onClick={handleCarClick}
					services={services}
					activeService={activeService}
				/>
			</aside>
		</article>
	);
}
