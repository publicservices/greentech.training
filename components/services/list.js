import cardStyles from '@/styles/components/services/card.module.css'
import Card from './card'

export default function List ({
	services,
	onClick,
	activeService,
}) {
	return (
		<ol className={cardStyles.listCards}>
			{services.map((service, index) => (
				<li key={index}>
					<Card
						active={activeService && service.slug === activeService.slug}
						service={service}
						key={service.slug}
						onClick={onClick}
					/>
				</li>
			))}
		</ol>
	)
}
