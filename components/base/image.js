import {useRef, useEffect, useState} from 'react'
import clsx from 'clsx'

import config from '@/next.config.js'

import styles from '@/styles/components/img.module.css'

export default function BaseImage({
	src,
	className,
}) {
	const image = useRef(null)
	const [isLoaded, setIsLoaded] = useState(false)

	const handleLoad = () => setIsLoaded(true)

	useEffect(() => {
		if (image.current.complete) setIsLoaded(true)
	}, [])

	return (
		<img
			className={clsx(
				className,
				styles.image,
				isLoaded ? styles.imageIsLoaded : styles.imageIsLoading
			)}
			loading='lazy'
			src={`${config.basePath}/${src}`}
			ref={image}
			alt={`${src ? src.match(/[\w-]+.(jpg|png|jpeg|svg|webP|bmp|jfif|.ico|tif|tiff)/g).toString().replace(/(\.[a-z]*)/,'') : null}`}
			onLoad={handleLoad}
		/>
	)
}
