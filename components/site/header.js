import styles from "@/styles/components/site/header.module.css";
import Link from "next/link";
import Menu from "./menu";
import Image from "next/image";
import React, { useRef, useState, useEffect, useCallback } from "react";
import greentechLogo from "@/public/media/logos/green-logo.svg";
import userLogo from "@/public/media/logos/user-logo.png";
function Header({ className, siteMenu, footerMenu, socialMenu }) {
	const [hidden, setHidden] = useState(true);
	const [windowSize, setWindowSize] = useState(null);
	const ref = useRef(null);
	const handleClick = () => {
		setHidden(!hidden);
	};
	const maxYPixForMobileMenu = 1470;

	/* this function is to automatically hide the mobile if the mobile menu is visible and the screen widh get bigger then 1280px
	to avoid the miss behavior when the hamberger button gets clicked then the user resize the screen to bigger then 1280px
	*/
	const handleWindowResize = useCallback((event) => {
		setWindowSize(window.innerWidth);
	}, []);
	useEffect(() => {
		window.addEventListener("resize", handleWindowResize);
		if (windowSize > maxYPixForMobileMenu) setHidden(true);

		return () => {
			window.removeEventListener("resize", handleWindowResize);
		};
	}, [handleWindowResize, windowSize]);

	/* set the background of the hole body to dark on hamburger menu open*/
	useEffect(() => {
		if (!hidden) {
			// set a brightness filter to the main tag inside the body
			document.getElementsByTagName("main")[0].style.filter = "brightness(50%)";
			//animate the darkening effect to be smooth
			document.getElementsByTagName("main")[0].style.transition =
				"all 1s ease-in-out";
		} else {
			// reset the brightness filter to 100% when the menu is closed
			document.getElementsByTagName("main")[0].style.filter =
				"brightness(100%)";
			//animate the darkening effect to be smooth
			document.getElementsByTagName("main")[0].style.transition =
				"all 1s ease-in-out";
		}
	}, [hidden]);

	/* disable scrolling when the hamburger menu is open*/
	useEffect(() => {
		if (!hidden) {
			document.body.style.overflow = "hidden";
		} else {
			document.body.style.overflow = "scroll";
		}
	}, [hidden]);

	/**
	 * close the hambuger if clicked on outside of it
	 */
	useEffect(() => {
		function handleClickOutside(event) {
			if (ref.current && !ref.current.contains(event.target)) {
				setHidden(true);
			}
		}
		// Bind the event listener
		window.addEventListener("mousedown", handleClickOutside);
		return () => {
			// Unbind the event listener on clean up
			window.removeEventListener("mousedown", handleClickOutside);
		};
	}, [ref]);
	return (
		<div id="nav-bar" ref={ref} className={className}>
			<nav className={styles.root}>
				<h1 className={styles.title}>
					<Link href="/" passHref>
						<a>
							<Image
								className={styles.logo}
								alt="greentech logo"
								src={greentechLogo}
							></Image>
						</a>
					</Link>
				</h1>
				<div className={styles.menuItem}>
					<Menu menu={siteMenu} />
				</div>

				<div className={styles.menuItem}>
					<div className={styles.loginBlock}>
						<Image alt="user-logo" layout="fixed" src={userLogo}></Image>
						<Link href="/login">My Greentech login</Link>
					</div>
				</div>

				<div className={styles.hamburger}>
					<button onClick={handleClick} className={styles.hamburgerBtn}>
						<svg
							width="25"
							height="24"
							viewBox="0 0 25 24"
							fill="none"
							xmlns="http://www.w3.org/2000/svg"
						>
							<path
								d="M21 4.5H3"
								stroke="white"
								strokeWidth="2"
								strokeLinecap="round"
								strokeLinejoin="round"
							/>
							<path
								d="M23 12H11"
								stroke="white"
								strokeWidth="2"
								strokeLinecap="round"
								strokeLinejoin="round"
							/>
							<path
								d="M21 19.5L3 19.5"
								stroke="white"
								strokeWidth="2"
								strokeLinecap="round"
								strokeLinejoin="round"
							/>
						</svg>
					</button>
				</div>
			</nav>
			<div
				className={hidden ? styles.hamburgerMenu : styles.hamburgerMenuActive}
			>
				<div className={styles.hamburger}>
					<button onClick={handleClick} className={styles.hamburgerBtn}>
						<svg
							width="24"
							height="24"
							viewBox="0 0 24 24"
							fill="none"
							xmlns="http://www.w3.org/2000/svg"
						>
							<path
								d="M18 6L6 18"
								stroke="#014614"
								strokeWidth="2"
								strokeLinecap="round"
								strokeLinejoin="round"
							/>
							<path
								d="M6 6L18 18"
								stroke="#014614"
								strokeWidth="2"
								strokeLinecap="round"
								strokeLinejoin="round"
							/>
						</svg>
					</button>
				</div>
				<div className={styles.loginBlockMobile}>
					<svg
						width="18"
						height="18"
						viewBox="0 0 18 18"
						fill="none"
						xmlns="http://www.w3.org/2000/svg"
					>
						<g clipPath="url(#clip0_1145_2711)">
							<path
								d="M14.4002 17.9992V16.3992C14.4002 15.5505 14.0525 14.7366 13.4336 14.1365C12.8148 13.5364 11.9754 13.1992 11.1002 13.1992H4.5002C3.62498 13.1992 2.78561 13.5364 2.16674 14.1365C1.54787 14.7366 1.2002 15.5505 1.2002 16.3992V17.9992"
								stroke="#014614"
								strokeWidth="2"
								strokeLinecap="round"
								strokeLinejoin="round"
							/>
							<path
								d="M8.3998 9.60039C10.388 9.60039 11.9998 7.98862 11.9998 6.00039C11.9998 4.01217 10.388 2.40039 8.3998 2.40039C6.41158 2.40039 4.7998 4.01217 4.7998 6.00039C4.7998 7.98862 6.41158 9.60039 8.3998 9.60039Z"
								stroke="#014614"
								strokeWidth="2"
								strokeLinecap="round"
								strokeLinejoin="round"
							/>
						</g>
						<defs>
							<clipPath id="clip0_1145_2711">
								<rect width="18" height="18" fill="white" />
							</clipPath>
						</defs>
					</svg>
					<Link href="/login">My Greentech login</Link>
				</div>

				<Menu
					isMobileView
					menu={siteMenu}
					footerMenu={footerMenu}
					socialMenu={socialMenu}
				/>
			</div>
		</div>
	);
}

export default Header;
