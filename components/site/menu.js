import styles from "@/styles/components/site/menu.module.css";
import Link from "next/link";
import Footer from "./footer";
function MenuItem({ item }) {
	const { title, url } = item;
	if (url) {
		if (url.startsWith("https://")) {
			return (
				<a href={url} target="_blank" rel="noreferrer noopener">
					{title}
				</a>
			);
		} else {
			return <Link href={`/${url}`}>{title}</Link>;
		}
	} else {
		return <a href={`#`}>{title}</a>;
	}
}

function Menu({ menu, isMobileView, footerMenu, socialMenu }) {
	return menu?.frontmatter?.links?.length ? (
		<>
			<menu
				title={`Site menu`}
				className={isMobileView ? styles.mobileMenuView : styles.root}
			>
				{menu.frontmatter.links.map((item, index) => (
					<li key={index} className={styles.item}>
						<MenuItem item={item} />
					</li>
				))}
			</menu>
			<div className={styles.mobileFooterMenuView}>
				<Footer menu={footerMenu} />
			</div>
		</>
	) : (
		<menu className={styles.root}>
			<li className={styles.item}>
				<Link href="/login">My Greentech login</Link>
			</li>
		</menu>
	);
}

export default Menu;

export { MenuItem };
