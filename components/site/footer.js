import styles from "@/styles/components/site/menu.module.css";

import Link from "next/link";
import { MenuItem } from "./menu";

function Footer({ menu, socialMenu }) {
	return (
		<>
			{menu ? (
				<menu className={styles.root}>
					{menu.frontmatter.links.map((item, index) => (
						<li key={index} className={styles.item}>
							<MenuItem item={item} />
						</li>
					))}
				</menu>
			) : null}
			{socialMenu ? (
				<menu className={styles.root}>
					{socialMenu.frontmatter.links.map((item, index) => (
						<li key={index} className={styles.item}>
							<MenuItem item={item} />
						</li>
					))}
				</menu>
			) : null}
		</>
	);
}

export default Footer;
