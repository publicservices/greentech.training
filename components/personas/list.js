import cardStyles from '@/styles/components/personas/card.module.css'
import Card from './card'

export default function List ({personas}) {
	return (
		<div className={cardStyles.listCards}>
			{personas.map(personae => (
				<Card personae={personae} key={personae.slug}/>
			))}
		</div>
	)
}
