import styles from '@/styles/components/personas/card.module.css'
import Link from 'next/link'

export default function Card ({personae}) {
	const {
		slug,
		title,
		subtitle,
	} = personae.frontmatter
	return (
		<article className={styles.card}>
			<Link href={`/join/${slug}`}>
				<a>
					<header className={styles.header}>
						<h2 className={styles.title}>
							{title}
						</h2>
						<p className={styles.subtitle}>
							{subtitle}
						</p>
					</header>
				</a>
			</Link>
		</article>
	)
}
