import {useRef} from 'react'
import clsx from 'clsx'
import useElementOnScreen from '@/hooks/element-on-screen'

import styles from '@/styles/components/sections/layout.module.css'
function Layout({
	children,
	fullHeight,
	transparent,
	title,
	padding,
	className,
	bgLight,
	bgHighlight,
	bgTheme,
	radius,
}) {
	const containerRef = useRef(null)
	const isVisible = useElementOnScreen(containerRef, {
		root: null,
		rootMargin: '1px',
		threshold: 0.4,
	})
	return (
		<section className={clsx(
			styles.section,
			isVisible && styles.sectionIsVisible,
			fullHeight && styles.sectionFullHeight,
			transparent && styles.sectionTransparent,
			padding && styles.sectionPadding,
			className && styles[className],
			className,
			bgLight && styles.sectionBgLight,
			bgHighlight && styles.sectionBgHighlight,
			bgTheme && styles.sectionBgTheme,
			radius && styles.sectionRadius,
		)} ref={containerRef}>
			{title ? (
				<header className={styles.title}>
					{title}
				</header>
			) : null}
			{children}
		</section>
	)
}

export default Layout
