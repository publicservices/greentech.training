import { useRef, useState, useEffect } from "react";
import Header from "./site/header.js";
import Footer from "./site/footer.js";
import Menu from "./site/menu.js";

import styles from "@/styles/components/layout.module.css";
import headerStyles from "@/styles/components/site/header.module.css";
function Layout({ children, siteMenu, footerMenu, socialMenu }) {
	const [navBackground, setNavBackground] = useState(null);
	const [prevScrollpos, setPrevScrollpos] = useState(0);
	const [image, setImage] = useState("");

	//try to find if there is a background image in the page
	// if there is an image then set the background initial bg to transparent
	//if not then set the background of the nav bar  to green
	useEffect(() => {
		const getBgImage = () => {
			const bgImage = document.getElementById("bg-image");
			if (!bgImage) {
				setImage(null);
				setNavBackground("headerWrapperVisibleDark");
			} else {
				setImage(bgImage);
			}
		};
		getBgImage();
	}, [image]);

	//show/hide header on scroll
	useEffect(() => {
		const handleScroll = () => {
			const maxYposForTransparentBg = 100;
			const currentScrollPos = window.pageYOffset;

			if (prevScrollpos < maxYposForTransparentBg) {
				if (image === null) {
					setNavBackground("headerWrapperVisibleDark");
				} else {
					setNavBackground("headerWrapperVisible");
				}
			}
			if (prevScrollpos >= maxYposForTransparentBg) {
				setNavBackground(
					prevScrollpos > currentScrollPos
						? "headerWrapperVisibleDark"
						: "headerWrapper"
				);
			}
			setPrevScrollpos(currentScrollPos);
		};
		document.addEventListener("scroll", handleScroll);
		return () => {
			document.removeEventListener("scroll", handleScroll);
		};
	}, [image, prevScrollpos]);

	// After using one form for all pages and in order to make it usefull for all pages even in different cases
	// we have to show and hide some html elements
	// Hide the cef levels radio buttons from the form at the unwanted pages
	useEffect(() => {
		if(["/partner","/contact"].includes(location.pathname.slice(location.pathname.lastIndexOf("/")))){
			const cef_levels = document.getElementById("cef_levels");
			cef_levels.style.display = "none";
			const nationality = document.getElementById("nationality");
			nationality.style.display = "none";
			const current_location = document.getElementById("current_location");
			current_location.style.display = "none";

			const text_title = document.getElementById("text_title");
			text_title.style.marginBottom = "2.5rem";
		}
	});

	return (
		<>
			<Header
				footerMenu={footerMenu}
				socialMenu={socialMenu}
				siteMenu={siteMenu}
				className={
					navBackground === "headerWrapper"
						? headerStyles.headerWrapper
						: navBackground === "headerWrapperVisibleDark"
						? headerStyles.headerWrapperVisibleDark
						: headerStyles.headerWrapperVisible
				}
			/>

			<main className={styles.main}>{children}</main>

			<footer className={styles.footer}>
				<Footer menu={footerMenu} socialMenu={socialMenu} />
			</footer>
		</>
	);
}

export default Layout;
