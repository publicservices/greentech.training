import styles from "@/styles/components/sections/join-offer.module.css";
import Image from "@/components/base/image";
import Card from "../services/card";

export default function JoinOffer({ offer }) {
	//console.log(offer.blocks);
	return (
		<article className={styles.section}>
			<header className={styles.header}>
				<h1 className={styles.title}>{offer.title}</h1>
			</header>
			{offer.blocks ? (
				<main className={styles.blocks}>
					{offer.blocks.map((block, index) => {
						return (
							<article key={index} className={styles.block}>
								<header className={styles.blockHeader}>
									<h2 className={styles.blockTitle}>{block.title}</h2>
								</header>
								<main className={styles.blockContent}>{block.body}</main>
							</article>
						);
					})}
				</main>
			) : null}
		</article>
	);
}
