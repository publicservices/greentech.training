import styles from "@/styles/components/sections/homepage-concept.module.css";
import Image from "@/components/base/image";
import Link from "next/link";

export default function SectionConcept({ blocks }) {
	const title = blocks.find((s) => s.type === "title");
	const content = blocks.find((s) => s.type === "content");
	const image = blocks.find((s) => s.type === "image");
	return (
		<article className={styles.root}>
			<div className={styles.contentContainer}>
				<h1 className={styles.title}>{title.content}</h1>
				<main className={styles.content}>{content.content}</main>
			</div>
			<div className={styles.header}>
				{image ? (
					<figure className={styles.imageWrapper}>
						<div id="bg-image">
							<Image
								className={styles.image}
								src={`${image.src}`}
								title={image.title}
							/>
						</div>
					</figure>
				) : null}
			</div>
		</article>
	);
}
