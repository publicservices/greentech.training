import clsx from "clsx";
import { useRef } from "react";

import useElementOnScreen from "@/hooks/element-on-screen";
import Image from "@/components/base/image";
import Link from "next/link";

import styles from "@/styles/components/sections/section-intro.module.css";

export default function SectionIntro({ blocks }) {
	const title = blocks.find((s) => s.type === "title");
	const content = blocks.find((s) => s.type === "content");
	const image = blocks.find((s) => s.type === "image");

	const containerRef = useRef(null);
	const isVisible = useElementOnScreen(containerRef, {
		root: null,
		rootMargin: "1px",
		threshold: 0.8,
	});

	return (
		<article
			className={clsx(styles.section, isVisible && styles.sectionIsVisible)}
			ref={containerRef}
		>
			<header className={styles.header}>
				<h1 className={styles.title}>{title.content}</h1>
				<main className={styles.content}>{content.content}</main>
			</header>
			<aside className={styles.aside}>
				<div id="bg-image"></div>
				<Image src={image.src}></Image>
			</aside>
		</article>
	);
}
