import styles from "@/styles/components/sections/course-concept.module.css";
import Image from "@/components/base/image";
import Link from "next/link";

export default function JoinConcept({ title, content }) {
	return (
		<article className={styles.section}>
			<h1 className={styles.title}>{title}</h1>
			<main
				className={styles.content}
				dangerouslySetInnerHTML={{ __html: content }}
			></main>
		</article>
	);
}
