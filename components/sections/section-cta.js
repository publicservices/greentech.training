import clsx from "clsx";
import { useRef } from "react";

import useElementOnScreen from "@/hooks/element-on-screen";
import Image from "@/components/base/image";
import Link from "next/link";

import styles from "@/styles/components/sections/section-cta.module.css";

export default function SectionCta({ blocks }) {
	const containerRef = useRef(null);
	const isVisible = useElementOnScreen(containerRef, {
		root: null,
		rootMargin: "1px",
		threshold: 0.4,
	});

	const content = blocks.find((s) => s.type === "content");
	const cta = blocks.find((s) => s.type === "cta");
	const image = blocks.find((s) => s.type === "image");

	return (
		<article
			className={clsx(styles.root, isVisible && styles.rootIsVisible)}
			ref={containerRef}
		>
			<main className={styles.content}>{content.content}</main>
			<Link href={`/${cta.link}`}>
				<a className={styles.cta}>{cta.title}</a>
			</Link>
			<div id="bg-image">
				<Image src={image.src}></Image>
			</div>
		</article>
	);
}
