import styles from "@/styles/components/courses/card.module.css";
import Link from "next/link";
import Image from "next/image";
import config from "@/next.config.js";
import BaseImage from "../base/image";
export default function Card({ course }) {
	const { slug, title, description_short, duration, src,text_form } = course.frontmatter;
	console.log(course);
	return (
		<article className={styles.root}>
			<Link href={`/courses/${slug}`}>
				<a>
					<header className={styles.header}>
						<BaseImage
							className={styles.baseImage}
							alt="course-image"
							src={src}
						/>

						<h2 className={styles.title}>{title}</h2>
						<p className={styles.duration}>{duration}</p>
					</header>
					<main className={styles.content}>{description_short}</main>
					<footer className={styles.footer}>
						<button className={styles.btn}>Mehr Erfahren</button>
					</footer>
				</a>
			</Link>
		</article>
	);
}
