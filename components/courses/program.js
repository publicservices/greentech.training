import styles from "@/styles/components/courses/course.module.css";

const ProgramLine = ({ program }) => {
	const { title, identifier, body } = program;
	return (
		<article className={styles.program}>
			<aside className={styles.programIdentifier}>{identifier}</aside>
			<main className={styles.programMain}>
				{title ? <h3 className={styles.programTitle}>{title}</h3> : null}
				{body ? <div className={styles.programContent}>{body}</div> : null}
			</main>
		</article>
	);
};

export default function Program({ program }) {
	return (
		<menu className={styles.programMenu}>
			<li className={styles.programLi}>
				<ProgramLine
					program={{
						identifier: "Woche",
						title: "Inhalt",
					}}
				/>
			</li>
			{program.map((item, index) => (
				<li className={styles.programLi} key={index}>
					<ProgramLine program={item} />
				</li>
			))}
		</menu>
	);
}
