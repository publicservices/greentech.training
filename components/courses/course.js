import pageStyles from '@/styles/pages/course.module.css'
import Link from 'next/link'

import Program from './program'

export default function Course ({course}) {
	const {slug, frontmatter, contentHtml} = course
	const {
		title,
		content,
		description_short,
		duration,
		form,
		program
	} = frontmatter
	return (
		<article className={pageStyles.root}>
			{/* <header className={pageStyles.header}>
					<h1 className={pageStyles.title}>
					<Link href={`/courses/${slug}`}>{title}</Link>
					</h1>
					</header> */}
			{/* <main className={pageStyles.content} dangerouslySetInnerHTML={{ __html: contentHtml }}></main> */}
			<section>
				{program ? (
					<Program program={program}/>
				) : null}
			</section>
		</article>
	)
}
