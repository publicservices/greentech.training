import cardStyles from '@/styles/components/courses/card.module.css'
import Card from './card'

export default function List({
	courses,
}) {
	return (
		<div className={cardStyles.listCards}>
			{courses.map(course => (
				<Card course={course} key={course.slug}/>
			))}
		</div>
	)
}
