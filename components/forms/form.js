import text_form from "@/public/admin/fields/text_form";
import styles from "@/styles/components/sections/form.module.css"

// TODO - this is no valid solution as it does not support multiple languages...
//        we will postpone it for the next sprint though..
export default function Form({form}) {
    const {slug,frontmatter, contentHtml} = form
	const text_form= frontmatter.text_form
    const text_title = frontmatter.text_title
    const newsletter_text = frontmatter.newsletter_text
    const firstname_key = frontmatter.firstname_key
    const lastname_key = frontmatter.lastname_key
    const email_key = frontmatter.email_key
    const cef_key = frontmatter.cef_key
    const nationality_key = frontmatter.nationality_key
    const location_key = frontmatter.location_key
    const message_key = frontmatter.message_key
    const submit_key = frontmatter.submit_key
	return (
		<div className={styles.container}>
			<div className={styles.wrapper_contact_form}>
			<div className={styles.wrapper_contact_form_text}>
			<h1 id="text_title" className={styles.title}>{text_title}</h1>
			     <p className={styles.paragraph}>
				   {text_form}
                 </p>
			</div>
			<form className={styles.contact_form} action="https://training.us9.list-manage.com/subscribe/post?u=60315edcea0b2b9b146680e16&amp;id=1ea1e852a2&amp;f_id=00a608e1f0" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" target="_self">
					<div className={styles.wrapper_contact_form_fields}>
					<div className={styles.vNameNachname}>
						<label htmlFor="mce-FNAME">
							{firstname_key}
							<input
								type="text"
								name="FNAME"
								id="mce-FNAME"
								required
							/>
						</label>
						<label htmlFor="mce-LNAME">
							{lastname_key}
							<input
								type="text"
								name="LNAME"
								id="mce-LNAME"
								required
							/>
						</label>
					</div>
					<label htmlFor="mce-EMAIL">
						{email_key}
						<input

							type="email"
							name="EMAIL"
							id="mce-EMAIL"
							required
						/>
					</label>
					<label id="cef_levels" htmlFor="mce-FNAME">
						{cef_key}
						<div className={styles.cef_level_wrapper}>
						    <input type="radio" value="A2.3" name="MMERGE7" id="mce-MMERGE7-0"/>
							<label htmlFor="mce-MMERGE7-0">A2.3</label>
							<input type="radio" value="B1.1" name="MMERGE7" id="mce-MMERGE7-1"/>
							<label htmlFor="mce-MMERGE7-1">B1.1</label>
							<input type="radio" value="B1.2" name="MMERGE7" id="mce-MMERGE7-2"/>
                            <label htmlFor="mce-MMERGE7-2">B1.2</label>
							<input type="radio" value="B1.3" name="MMERGE7" id="mce-MMERGE7-3"/>
                            <label htmlFor="mce-MMERGE7-3">B1.3</label>
							<input type="radio" value="B2+" name="MMERGE7" id="mce-MMERGE7-4"/>
                            <label htmlFor="mce-MMERGE7-4">B2+</label>
						</div>
					</label>
					<div className={styles.vNameNachname}>
						<label id="nationality" htmlFor="mce-MMERGE8">{nationality_key}
	                    <input type="text" name="MMERGE8"  id="mce-MMERGE8"/>
						</label>
						<label id="current_location" htmlFor="mce-MMERGE9">{location_key}
	                    <input type="text" name="MMERGE9" id="mce-MMERGE9"/>
						</label>
					</div>
					<label id="mail_message" htmlFor="mce-MMERGE6">
                    {message_key}
						<textarea
							style={{ width: "100%" }}
							name="MMERGE6"
                            rows="12"
							id="mce-MMERGE6"
							required
						/>
					</label>
				    <div className={styles.wrapper}>
					<label id="newsletter">
					<input style={{width:'auto',marginLeft:"0"}} type="checkbox" name="checkbox"/>
					<span className={styles.span}>{newsletter_text}</span>
					</label>
					</div>
					<input type="text" style={{display:'none'}} value={`${globalThis?.window?.location?.href}`} readOnly  name="MMERGE10" id="mce-MMERGE10"/>
					<button
						className={styles.contact_submit}
						type="submit"
						id="mc-embedded-subscribe"
						value="Submit"
					>
						{submit_key}
					</button>
					<div
						style={{ position: "absolute", left: "-5000px" }}
						aria-hidden="true"
					>
						<input
							type="text"
							name="b_60315edcea0b2b9b146680e16_1ea1e852a2"
							tabIndex={-1}
						/>
					</div>
					</div>
				</form>
			</div>
		</div>
	);
}
