import {useState, useEffect} from 'react'

const useElementOnScreen = (
	elementRef,
	options = {
		root: null,
		rootMargin: '1px',
		threshold: 1,
	}
) => {
	const [isVisible, setIsVisible] = useState(false)

	const callbackFunction = (entries) => {
		const [entry] = entries
		setIsVisible(entry.isIntersecting)
	}

	useEffect(() => {
		const observer = new IntersectionObserver(callbackFunction, options)
		if (elementRef.current) observer.observe(elementRef.current)
		return () => {
			if (elementRef.current) observer.unobserve(elementRef.current)
		}
	}, [elementRef, options])

	return isVisible
}

export default useElementOnScreen
