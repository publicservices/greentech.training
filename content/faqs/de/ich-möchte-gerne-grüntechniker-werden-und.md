---
title: Ich möchte gerne grüner Techniker (m/w/d) werden und …
slug: trainee
questions:
  - title: will wissen, wie viel es kostet, sich für das greentech.training-Programm
      anzumelden
    body: >-
      Wenn Sie in Berlin sind: Während unseres Pilotprogramms ist die Teilnahme
      am Kurs in Berlin für die Teilnehmenden kostenlos. Wir übernehmen die
      Kosten als eine Investition in Sie und Ihre Zukunft - eine Zukunft, die
      von unserem Netzwerk von Konsortialpartnern unterstützt wird.


      Wenn Sie in einer anderen Stadt in Deutschland leben: Wir planen, das Programm ab Ende 2023 auch in anderen Städten anzubieten. Zunächst müssen wir sicherstellen, dass das Pilotprogramm gut funktioniert, bevor wir es in anderen Städten anbieten.
  - title: würde gerne in einem Bereich der erneuerbaren Energien arbeiten, der
      nicht die Solarenergie betrifft
    body: Obwohl wir uns mit unserem Pilotkurs auf den dringenden Bedarf der
      Gesellschaft an Solar Assistants (m/w/d) und Solar Monteuren (m/w/d)
      konzentrieren, kommen unsere Konsortialpartner aus verschiedenen Bereichen
      der grünen Wirtschaft. Unser Ziel ist es, eine Grundausbildung zu
      vermitteln und anschließend Kontakte zu Unternehmen herzustellen, die Ihre
      Karriere fördern können.
  - title: wohne nicht in Berlin
    body: Die Kurse Solar Assistant (m/w/d) und Solar Monteur (m/w/d) werden in
      Berlin pilotiert, wo sich unsere ersten Praxispartner befinden. Weitere
      Städte und Partner sind für Ende 2023 in Planung. Internationale
      Bewerberinnen und Bewerber mit soliden IT-Kenntnissen können sich über die
      Seite "Internationale Bewerberinnen und Bewerber" für den Kurs Solar
      Specialist (m/w/d) bewerben. Dieser Kurs wird komplett online absolviert.
  - title: habe keine Erfahrung
    body: Dann bewerben Sie sich für den Kurs Solar Assistant (m/w/d). Wir
      entwickeln klare Karrierewege, damit die Assistants sehen können, welche
      Karrieremöglichkeiten sie haben, wenn sie in unserem Netzwerk von
      Partnerunternehmen Erfahrungen gesammelt haben.
  - title: " habe noch kein CEF B1 Niveau in Deutsch erreicht"
    body: B1.2 ist eine Voraussetzung für alle zukünftigen grünen Techniker (m/w/d)
      in Deutschland. Internationale Bewerberinnen und Bewerber können sich
      bewerben, wenn sie über CEF 2.3 Deutsch und solide IT-Kenntnisse verfügen.
  - title: möchte wissen, welche Aufgaben ich als Solar Assistant (m/w/d) übernehmen
      kann
    body: >-
      Solar Assistants (m/w/d) können unter Anleitung von erfahrenem Personal
      eine begrenzte Anzahl von Aufgaben ausführen. Sie haben eine
      unterstützende Funktion bei Solarinstallationsmaßnahmen. Dazu können
      gehören:


      - Bedienen von Leitungsschutzschaltern und Fehlerstromschutzschaltern (RCDs)

      - Auswechseln von Sicherungseinsätzen und Lampen

      - Rückstellen von Not-Aus-Einrichtungen oder Schutzeinrichtungen wie Motorschutzschaltern
  - title: möchte wissen, welche Aufgaben ich als Solar Monteur (m/w/d) übernehmen
      kann
    body: >-
      Solar Monteure (m/w/d) können bestimmte, sich wiederholende
      Installationsarbeiten, die vom Arbeitgeber vorgegeben sind, selbständig
      ausführen. Sie dürfen an Anlagen mit Nennspannungen bis zu 1.000 V AC oder
      1.500 V DC arbeiten, allerdings nur im spannungslosen Zustand, z. B. das
      Befestigen und Montieren von Schalttafeln.


      Diese Position ist verantwortungsvoller als die eines Solar Assistants (m/w/d) und vermittelt die grundlegenden Fähigkeiten, die erforderlich sind, um ein voll qualifizierte Fachkraft für Elektrotechnik zu werden.
  - title: will wissen, ob ich mich mit meiner Qualifikation und Ausbildung als
      Solar Specialist (m/w/d) bewerben kann
    body: >-
      Während des Bewerbungsverfahrens können Sie deutsche Übersetzungen Ihrer
      Qualifikationen einreichen, wenn Sie diese aus Ihrem Heimatland haben,
      dies ist jedoch nicht unbedingt erforderlich. Wir unterstützen jedoch Ihr
      Anerkennungsverfahren bei der IHK, wenn dies von den Konsortialpartnern
      gefordert wird. Wichtiger ist, dass Sie Berufserfahrung in einem der
      folgenden Bereiche nachweisen können:


      - als Dachdecker/in


      - als Elektriker/in


      - Berufe des Maschinenbaus und der Fahrzeugtechnik


      - Mechatronik-, Energie- und Elektroberufe


      - Bau- und Produktionssteuerungsberufe


      - Bauplanungs-, Architektur- und Vermessungsberufe


      - Bauberufe


      - Berufe der Kommunikationstechnik (IT)


      - Berufe der Gebäude- und Versorgungstechnik
  - title: möchte wissen, wer die praktischen Abschnitte der
      greentech.training-Programme durchführt
    body: >-
      In den meisten EU-Ländern gibt es klar definierte Richtlinien, wer
      elektrische Geräte bedienen darf. In Deutschland sind diese in den
      folgenden Bestimmungen festgelegt:


      - dem Gesetz zur Regelung der handwerklichen Tätigkeiten (Handwerksordnung)


      - die Unfallverhütungsvorschrift "Elektrische Anlagen und Betriebsmittel" (DGUV Vorschrift 3) mit den dazugehörigen Durchführungsanweisungen und


      - die Grundsätze der Berufsgenossenschaft: Ausbildungskriterien für festgelegte Tätigkeiten gemäß der Durchführungsanweisung zur Unfallverhütungsvorschrift "Elektrische Anlagen und Betriebsmittel" (BGG 944).


      Um die Einhaltung dieser Protokolle zu gewährleisten, arbeiten wir nur mit bewährten Partnern zusammen, die für die Durchführung der angebotenen praktischen Ausbildungsleistungen qualifiziert sind. Außerdem bitten wir unsere Teilnehmerinnen und Teilnehmer, uns ein Feedback zu ihrer Ausbildung bei den Partnern zu geben, um eine positive Serviceerfahrung im gesamten Ausbildungsnetzwerk zu gewährleisten.
  - title: möchte wissen, ob ich mich als Deutsch-Muttersprachler/in bewerben kann
    body: Ja. Da unser Kurs darauf abzielt, Nicht-Muttersprachler/innen in das
      Arbeitsleben in Deutschland einzuführen und ihnen bei der Integration zu
      helfen, begrüßen wir die Teilnahme von Muttersprachlerinnen und
      Muttersprachlern am Berliner Programm, die als Sprachrohr und Ratgeber vor
      Ort fungieren. Wir bemühen uns, ein bestimmtes Verhältnis von
      Nicht-Muttersprachlern zu Muttersprachlern pro Klasse zu erreichen, aber
      das ist nicht immer möglich.
  - title: möchte wissen, ob ich eine Ausbildung bei greentech.training beginnen kann
    body: Nicht direkt. Unsere Plattform ist darauf ausgerichtet, die Lücke zwischen
      willigen und fähigen Arbeitskräften für unsere Partner im Bereich der
      grünen Energie zu schließen. Das heißt, wenn Sie ein
      greentech.training-Grundlagenprogramm erfolgreich absolvieren, verbinden
      wir Sie mit unserem Netzwerk von Unternehmen, die bei Bedarf eine weitere
      qualifizierte Ausbildung anbieten können. Die Partnerunternehmen, die
      ausbildungsberechtigt sind, können auf ihrem Profil mit diesem Status
      werben, um neue grüne Fachkräfte zu gewinnen. Beachten Sie jedoch, dass
      nicht alle Partner Ausbildungsplätze anbieten.
---
