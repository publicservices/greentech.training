---
title: Mein Unternehmen ist daran interessiert, ab 2023 Arbeitskräfte
  einzustellen, und wir würden gerne wissen ...
slug: partners
questions:
  - title: wie greentech.training die Bewerberinnen und Bewerber für sein Programm
      auswählt
    body: Zunächst prüfen wir den aktuellen Stand der Deutschkenntnisse der
      Bewerberinnen und Bewerber. Dies kann zunächst durch ein Zertifikat
      nachgewiesen und per Videoanruf bestätigt werden. Die Sprachkenntnisse
      sowie frühere Ausbildungen und Erfahrungen bestimmen, welchen Weg die
      Teilnehmenden im Programm einschlagen. Wir besprechen dann die soziale
      Verantwortung, die Verfügbarkeit, den Standort und die persönlichen
      Eigenschaften der Bewerberinnen und Bewerber, um besser einschätzen zu
      können, ob sie in der Lage sind, das Programm zu absolvieren und
      langfristig im Partnerunternehmen zu bleiben. Auf Anfrage können wir auch
      erfahrene englischsprachige Technikerinnen und Techniker mit niedrigeren
      Deutschkenntnissen als B1 einstellen.
  - title: wie greentech.training seine Kursteilnehmerinnen und -teilnehmer
      qualifiziert
    body: greentech.training ist eine Plattform, die Arbeitskräfte für und mit dem
      grünen Energiesektor vorbereitet und verbindet. Dazu arbeiten wir mit
      etablierten Weiterbildungspartnern zusammen, die über die nötige
      Erfahrung, Autorisierung und Kundenbetreuung verfügen, um sicherzustellen,
      dass nur diejenigen mit national anerkannten Qualifikationen in unsere
      Datenbank für grüne Techniker (m/w/d) aufgenommen werden. Die einzelnen
      Bildungspartner sind abhängig vom jeweiligen Bundesland und der
      Verfügbarkeit, in dem das greentech.training-Programm angeboten wird. Für
      nähere Informationen zu aktuellen und geplanten lokalen Partnern und deren
      Programmen kontaktieren Sie uns bitte direkt. Personen mit früheren
      Qualifikationen werden in ihrem beruflichen Anerkennungsverfahren
      unterstützt, wenn solche Unterlagen von den Partnerunternehmen verlangt
      werden.
  - title: welche Art von Ökoenergie-Unternehmen beitreten können
    body: Unser Schwerpunkt liegt auf der Vorbereitung von Fachkräften für Elektro-
      und Datentechnik für die Arbeit bei unseren Konsortialpartnern in den
      Bereichen Solar, Wind, Wasserstoff und Geothermie. Wir akzeptieren auch
      Bewerbungen von Unternehmen, die ein klares und öffentliches Ziel
      verfolgen, um zur Verringerung der Kohlenstoffemissionen beizutragen.
      Dieses Ziel sollte Teil einer ganzheitlichen Strategie zur
      Kohlenstoffminimierung sein, die nicht die Verwendung von CO₂-Zertifikaten
      für den Kohlenstoffausgleich beinhaltet. Die Strategie zur
      Kohlenstoffminimierung sollte Teil einer Haupteinnahmequelle sein, die dem
      Zweck und der allgemeinen Absicht des §1 EEG 2021 dient (oder ausdrücklich
      zu dienen beabsichtigt). Bitte kontaktieren Sie uns, um zu besprechen, ob
      Ihr Unternehmen einen Antrag stellen kann. Wir führen eine kostenlose
      Ersteinschätzung durch.
  - title: wie viel es kostet, Mitglied zu werden
    body: Während des Pilotprogramms ist die Mitgliedschaft für aufgenommene
      Konsortialpartner kostenlos. Gebühren werden nur für die Vermittlung von
      grünen Technikern (m/w/d) erhoben. Außerdem bieten wir kostenpflichtige
      Integrationsworkshops, Unterstützung bei der Visumserteilung und
      Sprachunterricht an, um eine harmonische Eingliederung zu gewährleisten.
  - title: was passiert, wenn wir mit einem / einer über greentech.training
      eingestellten Arbeitnehmenden unzufrieden sind
    body: >-
      Wir prüfen und akzeptieren Kandidatinnen und Kandidaten für das Programm
      auf der Grundlage mehrerer Faktoren. Dazu gehören Zuverlässigkeit,
      Arbeitserlaubnis, die Wahrscheinlichkeit, das Programm abzuschließen,
      nachgewiesene soziale Verantwortung und die Fähigkeit der Bewerberinnen
      und Bewerber, langfristig bei einem Konsortialpartner zu bleiben. Das
      heißt, wenn ein grüner Techniker (m/w/d) in unsere Datenbank aufgenommen
      wird, ist ein Kunde, dem wir eine Sorgfaltspflicht schulden. Aus diesem
      Grund bieten wir den Partnerunternehmen Integrationsschulungen an, um eine
      reibungslose Aufnahme zu gewährleisten.


      Trotz dieser Maßnahmen kann es manchmal zu Streitigkeiten kommen. Bei Konflikten in Bezug auf Leistung und Zuverlässigkeit führen wir eine 360°-Mediation durch, um die Ursache zu ermitteln. Wenn wir der Meinung sind, dass die optimale Lösung für alle Parteien darin besteht, einen zweiten grünen Techniker (m/w/d) einzusetzen, werden wir dies kostenlos tun. Wir bieten auch eine 360°-Vermittlung an, wenn es zu einem zweiten Streitfall kommt, um sicherzustellen, dass das Problem der wiederholten Zurückhaltung nicht von Unternehmensseite aus gelöst werden kann. Wenn dies nicht möglich ist oder wenn wir der Meinung sind, dass den Konsortialpartner erneut keine Schuld trifft, bieten wir eine volle Geld-zurück-Garantie oder einen dritten Ersatz an, je nachdem, was der Konsortialpartner bevorzugt.
  - title: ob wir Geflüchtete mit nicht dauerhaftem Status einstellen können
    body: Eines der Ziele von greentech.training ist es, dem Arbeitskräftemangel in
      Deutschland zu begegnen. Aus diesem Grund haben wir ein Integrationsbüro
      mit Sozialarbeitern eingerichtet, die sowohl den Konsortialpartner als
      auch den grünen Techniker (m/w/d) mit einer befristeten Arbeitserlaubnis
      im Rahmen einer Verlängerung oder eines Antrags auf eine volle
      Arbeitserlaubnis beraten und unterstützen können. Dieser Service ist für
      das erste (Pilot-)Jahr kostenlos.
  - title: mit wem wir über eine Mitgliedschaft sprechen oder einen Vorschlag machen
      können
    body: "Bitte kontaktieren Sie uns: info@greentech.training."
  - title: ob greentech.training in meine Stadt kommen wird
    body: Bitte nehmen Sie zunächst Kontakt mit uns auf, um Ihr Interesse an der
      Teilnahme am Programm zu bekunden, und aktivieren Sie das Kontrollkästchen
      für die Newsletter-Anmeldung. Wir werden Ihre Anforderungen besprechen und
      feststellen, wann ein Kursbeginn in Ihrer Nähe möglich ist.
  - title: ob greentech.training in Zukunft weitere Kurse außerhalb des
      Energiebereiches anbieten wird
    body: Ja. Unser langfristiges Ziel ist es, eine Datenbank mit qualitativ
      hochwertigen Online-Kurzkursen anzubieten, die sich auf die Vorbereitung
      von Umwelttechnikern (m/w/d) konzentrieren, die dazu beitragen, die Welt
      zu einem kühleren und saubereren Ort zu machen. Folgen Sie uns auf
      Linkedin, um über Neuigkeiten in diesem Bereich informiert zu werden.
---
