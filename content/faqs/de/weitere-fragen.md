---
title: Weitere Fragen
slug: other-questions
questions:
  - title: Ich bin daran interessiert, greentech als Trainerin / Trainer,
      Freiwillige / Freiwilliger, Investorin / Investor oder in einer anderen
      Funktion zu unterstützen.
    body: Bitte kontaktieren Sie uns über das Kontaktformular.
---
