---
title: Solar Specialist (m/w/d)
slug: solar-specialist
description_short: Teilnehmerinnen und Teilnehmer mit Berufserfahrung aus ihrem
  Heimatland haben die Möglichkeit, ein Hospitation bei einem unserer
  Konsortialpartner zu absolvieren. Das können z. B. Elektroingenieure,
  Klempnermeister, IT-Spezialisten, Dachdecker oder Elektriker (m/w/d) sein. Die
  Konsortiumsmitglieder können ein dreitägiges oder vierwöchige Hospitation
  anbieten. Mindestens ein Jahr Berufserfahrung ist erwünscht.
src: media/images/solar-specialist.jpg
subtitle: Solar Specialists (m/w/d) setzen sich mit ihrer Qualifikation und
  Erfahrung dafür ein, die Erderwärmung unter 1,5° zu halten
duration: Grundkurs + 3 Tage bzw. 4 Wochen Hospitation
program:
  - identifier: "1"
    title: Orientierung im deutschen Arbeitsmarkt (Grundkurs Block 1)
    body: In der ersten Woche befassen wir uns mit den Erwartungen und Pflichten von
      Arbeitnehmenden gegenüber ihren Arbeitgebenden und den Pflichten des
      Arbeitgebers gegenüber den Arbeitnehmenden. Außerdem befassen wir uns mit
      deutschen Industrienormen und Qualifikationsstandards sowie mit
      Arbeitsschutz, Arbeitsrecht, Unfallversicherung, Steuern und der
      wirtschaftlichen Bedeutung des Mittelstandes.
  - identifier: "2"
    title: Interkulturelle Kompetenz für grüne Techniker (m/w/d) (Grundkurs Block 2)
    body: In der zweiten Woche beschäftigen wir uns mit den unausgesprochenen Regeln
      am Arbeitsplatz, dem Verhalten gegenüber Kundinnen und Kunden, Kolleginnen
      und Kollegen, dem Führungsstil und dem Umgang mit Konflikten. Wir befassen
      uns auch mit der Bedeutung von Datensicherheit und die UN SDGs und
      Emissionsziele, die für die grüne Energiebranche relevant sind.
  - identifier: "3"
    title: Energiewende in der Fachrichtung Solar (Grundkurs Block 3)
    body: "In der dritten Woche geht es um Sonneneinstrahlung, Solarzellen und den
      Berliner Solarcity-Masterplan. Außerdem diskutieren wir über
      Solarenergie-Lieferketten und Netzanbindung in Deutschland,
      Sektorenkopplung und die Umweltauswirkungen der Solarenergie. "
  - identifier: 4+
    title: Hospitation von 3 Tagen / 4 Wochen oder Industrieanalyse
    body: "Teilnehmerinnen und Teilnehmer, die in der ersten Woche des Kurses eine
      Hospitation vereinbart haben, beginnen nun in ihrem jeweiligen Unternehmen
      entweder auf einer dreitägigen (Einblick) oder vierwöchigen (Einsatz)
      Basis. Die Möglichkeiten einer Hospitation hängen immer von den vorherigen
      Qualifikationen und den offenen Stellen der Partner ab. Diejenigen, die
      keine Hospitation vereinbart haben, führen ein Gruppenprojekt durch, das
      sich mit der Topologie der PV-Solaranlagenindustrie und deren Zusammenhang
      mit den deutschen Klimaneutralitätszielen beschäftigt. "
  - identifier: Nach Abschluss
    body: In der greentech.training-Datenbank sind Ihre bisherigen Erfahrungen für
      unsere Konsortialpartner einsehbar, einschließlich Ihrer Anerkennungen
      durch IHK oder HWK. Wenn Sie das Anerkennungsverfahren bei greentech
      begonnen haben, können Sie auch Ihre berufliche Anerkennung und Ihren
      Mängelbescheid hochladen, sobald dieser vorliegt. Auch ohne diese
      Unterlagen haben die Partner die Möglichkeit, Sie direkt zu einem
      Vorstellungsgespräch einzuladen und über die Plattform einzustellen.
is_featured: true
---
Wenn Sie in Ihrem Heimatland bereits Erfahrungen in Bereichen gesammelt haben, die von unseren Ökostrom-Konsortialpartnern gefordert werden, können Sie sich für den Lehrgang zum Solar Specialist (m/w/d) bewerben. Bewerben können sich Ingenieure, Klempnermeister, IT-Spezialisten, Dachdecker, Elektriker und erfahrene Handwerker (m/w/d). In der Regel ist ein Minimum an CEF B1.2 erforderlich, Ausnahmen sind jedoch möglich.

Am Ende des dreiwöchigen Grundkurses absolvieren die angehenden Solar Specialists entweder eine dreitägige Hospitation (Einblick) oder eine vierwöchige Hospitation (Einsatz). Sollte ein Job Shadowing zu diesem Zeitpunkt nicht möglich sein, wird eine Branchenanalyse durchgeführt, um das Bewusstsein dafür zu vertiefen, wo die individuellen Fähigkeiten in der Solarbranche eingesetzt werden können.

Erfolgreiche Absolventinnen und Absolventen werden dann in unsere Datenbank der grünen Techniker (m/w/d) aufgenommen, die in Vollzeitstellen bei einem unserer Partnerunternehmen für erneuerbare Energien integriert werden. Die Zahl der Stellen in dieser Fachrichtung ist begrenzt, da wir bestrebt sind, alle Absolventen innerhalb von zwei Monaten nach Abschluss des Kurses in die Solarbranche zu integrieren.

Eine HWK- oder IHK-Anerkennung ist nicht erforderlich, um in unsere Datenbank aufgenommen zu werden. Wir können Sie jedoch bei der beruflichen Anerkennung unterstützen und Ihren Defizitbescheid in Ihr Technikerprofil aufnehmen, damit Partnerunternehmen sehen können, welche Weiterbildungsmaßnahmen sie Ihnen anbieten müssen, um die Gleichwertigkeit mit der Industrie zu gewährleisten. Sie werden auch sehen, welche Unternehmen diese Ausbildungsberechtigung haben.

Bitte beachten Sie, dass sich die folgende Kursstruktur vor Beginn noch ändern kann. Im Laufe des Jahres 2022 und vor Kursbeginn im Jahr 2023 können wir auch versuchen, ausgewählte Fachkräfte bei Konsortialpartnern zur sofortigen Einstellung zu vermitteln.