---
title: International Trainee (m/w/d)
slug: international-trainee
description_short: Starten Sie Ihre Karriere als Motor der deutschen Energiewende
src: media/images/join-international-trainee-intro-v2.jpg
subtitle: Mit unserem Screening-, Schulungs- und Rekrutierungsprozess einer
  Festanstellung in Deutschland einen Schritt näher kommen
duration: 4 Wochen
program:
  - identifier: "1"
    title: Orientierung im deutschen Arbeitsmarkt (Grundkurs Block 1)
    body: In der ersten Woche befassen wir uns mit den Erwartungen und Pflichten von
      Arbeitnehmenden gegenüber ihren Arbeitgebern und den Pflichten des
      Arbeitgebers gegenüber den Arbeitnehmenden. Außerdem befassen wir uns mit
      deutschen Industrienormen und Qualifikationsstandards sowie mit
      Arbeitsschutz, Arbeitsrecht, Unfallversicherung, Steuern und der
      wirtschaftlichen Bedeutung des Mittelstandes.
  - identifier: "2"
    title: Interkulturelle Kompetenz für grüne Techniker (m/w/d) (Grundkurs Block 2)
    body: In der zweiten Woche beschäftigen wir uns mit den unausgesprochenen Regeln
      am Arbeitsplatz, dem Verhalten gegenüber Kundinnen und Kunden, Kolleginnen
      und Kollegen, dem Führungsstil und dem Umgang mit Konflikten. Wir befassen
      uns auch mit der Bedeutung von Datensicherheit und die UN SDGs und
      Emissionsziele, die für die grüne Energiebranche relevant sind.
  - identifier: "3"
    title: Energiewende in der Fachrichtung Solar (Grundkurs Block 3)
    body: In der dritten Woche geht es um Sonneneinstrahlung, Solarzellen und den
      Berliner Solar-Masterplan. Außerdem diskutieren wir über
      Solarenergie-Lieferketten und Netzanbindung in Deutschland,
      Sektorenkopplung und die Umweltauswirkungen der Solarenergie.
  - identifier: "4"
    title: Die Erstellung eines kleinen Prototyps in einer selbständig geführten
      Arbeitsgruppe.
    body: "In dieser Woche haben Sie die Aufgabe, eine digitale Lösung mit Daten
      über den Markt für erneuerbare Energien zu erstellen. Die Aufgabe muss als
      Team in einem Sprint ohne Aufsicht erledigt werden.  "
  - identifier: Nach Abschluss
    body: Eintrag in unsere geschlossene Datenbank für grüne Techniker (m/w/d), die
      nur registrierten und zugelassenen grünen Technologieunternehmen
      zugänglich ist, die nach vorgeprüften deutschsprachigen IT-Talenten
      suchen. Je nach Personalbedarf unseres Partners kann dies zu einem
      3-tägigen oder 4-wöchigen Remote-Praktikum oder einer sofortigen
      Remote-Anstellung führen.
is_featured: false
---
Deutschland befindet sich mitten in einer grünen Energiewende und hat sich das ehrgeizige Ziel gesetzt, bis 2045 klimaneutral zu werden. Bis 2030 sollen die Emissionen im Vergleich zu 1990 um mindestens 65 Prozent gesenkt werden. Um dies zu erreichen, werden qualifizierte IT- und Datensicherheitsexperten aus aller Welt benötigt.

greentech.training richtet sich an frischgebackene Informatik-Masterabsolventinnen und -absolventen sowie Berufserfahrene, um zunächst einen Einblick in die deutsche Arbeitskultur und ihre Standards zu erhalten, um zu zeigen, wie sie mit ihren Fähigkeiten Unternehmen im Bereich der erneuerbaren Energien unterstützen können, und um sie schließlich mit diesen Unternehmen für Remote-Praktika zusammenzubringen.

Wir nehmen Bewerbungen für einen Start im Frühjahr 2023 entgegen. Melden Sie Ihr Interesse an einem Kurs an, indem Sie das untenstehende Formular ausfüllen. Bitte fügen Sie in Ihrem Anschreiben einen Link zu Ihrem Git-Profil oder einem gleichwertigen Dokument bei. Auf der Grundlage Ihres Anschreibens können wir Sie bitten, eine vollständige Bewerbung mit Lebenslauf, Ausweis und Nachweis von Sprachkenntnissen einzureichen. Für die Bewerbung wird eine Gebühr von 19 € erhoben, in einigen Fällen können wir auch einen Videoanruf verlangen. Wenn Sie nach dieser Prüfung in den Kurs aufgenommen werden, wird eine Unterrichtsgebühr von 199 € fällig. Aktuelle oder ehemalige Studentinnen und Studenten unserer Partneruniversitäten haben Anspruch auf eine Ermäßigung. Setzen Sie sich mit uns in Verbindung, um zu erfahren, ob dies auf Sie zutrifft.

Wenn Sie angenommen werden, werden Sie während unseres 4-wöchigen Kurses Folgendes lernen. Er umfasst 8 Tage Online-Unterricht mit unserem Kursleiter. Beachten Sie, dass sich die Kursdetails vor Kursbeginn noch ändern können: