---
title: Solar Assistant (m/w/d)
slug: solar-assistant
description_short: Mit zertifizierten Partnern in Berlin bieten wir eine
  dreitägige Qualifizierung für ungelernte Bewerberinnen und Bewerber zur
  elektrotechnisch unterwiesenen Person (EuP) mit Zertifizierung in persönlicher
  Schutzausrüstung gegen Absturz (PSAgA) an. Es handelt sich um eine ungelernte
  Tätigkeit, die nur einen sehr eingeschränkten Zugang zu Stromkreisen
  ermöglicht, aber die Teilnehmer befähigt, PV-Installationsteams unter Aufsicht
  zu unterstützen. Keine Erfahrung erforderlich. CEF-B1.2 Minimum.
src: media/images/SA-min.jpg
subtitle: Mit unserem Screening-, Schulungs- und Rekrutierungsprozess einer
  Festanstellung in Deutschland einen Schritt näher kommen
duration: Grundkurs + 3 Tage
program:
  - identifier: "1"
    title: Orientierung im deutschen Arbeitsmarkt (Grundkurs Block 1)
    body: >
      In der ersten Woche befassen wir uns mit den Erwartungen und Pflichten von
      Arbeitnehmenden gegenüber ihren Arbeitgebern und den Pflichten des
      Arbeitgebers gegenüber den Arbeitnehmenden. Außerdem befassen wir uns mit
      deutschen Industrienormen und Qualifikationsstandards sowie mit
      Arbeitsschutz, Arbeitsrecht, Unfallversicherung, Steuern und der
      wirtschaftlichen Bedeutung des Mittelstandes. 
  - identifier: "2"
    title: Interkulturelle Kompetenz für grüne Techniker (m/w/d) (Grundkurs Block 2)
    body: In der zweiten Woche beschäftigen wir uns mit den unausgesprochenen Regeln
      am Arbeitsplatz, dem Verhalten gegenüber Kundinnen und Kunden, Kolleginnen
      und Kollegen, dem Führungsstil und dem Umgang mit Konflikten. Wir befassen
      uns auch mit der Bedeutung von Datensicherheit und die UN SDGs und
      Emissionsziele, die für die grüne Energiebranche relevant sind.
  - identifier: "3"
    title: Energiewende in der Fachrichtung Solar (Grundkurs Block 3)
    body: In der dritten Woche geht es um Sonneneinstrahlung, Solarzellen und den
      Berliner Solarcity-Masterplan. Außerdem diskutieren wir über
      Solarenergie-Lieferketten und Netzanbindung in Deutschland,
      Sektorenkopplung und die Umweltauswirkungen der Solarenergie.
  - identifier: "4"
    title: "3-tägiger Abschluss: Elektrotechnisch unterwiesene Person (EuP) mit
      Zertifizierung in Persönlicher Schutzausrüstung gegen Absturz (PSAgA)"
    body: Die letzte Woche verbringen wir bei unseren autorisierten
      Weiterbildungspartnern. Eine eintägige Schulung zur "EuP" zur
      Unterstützung der Kabelinstallation, gefolgt von einer zweitägigen
      Schulung zum Erlernen der korrekten Sicherheitsausrüstung und Verfahren
      zur Unterstützung der Installation von Solarmodulen auf Dächern.
  - identifier: Nach Abschluss
    body: Der Qualifizierung zum "EuP" und der Aufnahme in unsere Datenbank der
      grünen Techniker (m/w/d) steht den Konsortialpartnern zur Verfügung, die
      nach Vollzeit-Solar Assistants (m/w/d) suchen.
    title: ""
is_featured: true
---
In Zusammenarbeit mit Migrantenorganisationen und qualifizierten Bildungspartnern bereiten wir Technikerinnen und Techniker ohne Vorkenntnisse und mit mindestens B1.2 Deutsch auf die Weiterbildung zum Solar Assistant (m/w/d) vor.

Es handelt sich um eine Einstiegsposition, die es Ihnen ermöglicht, unter Aufsicht Solaranlagen zu installieren oder bei der Verkabelung in Kellern zu helfen. Viele unserer Konsortialpartner bieten auch Weiterbildungsmöglichkeiten an, so dass Sie auf Wunsch selbstständig als Solar Monteur (m/w/d) in deren Team arbeiten können.

Sie brauchen keine Vorkenntnisse, um den Kurs zu beginnen, aber wir verlangen ein Mindestniveau von CEF B1.2. Dies ermöglicht es Ihnen, die Kursinhalte zu verstehen und die damit verbundenen Aktivitäten online und im Unterricht durchzuführen. Der Grundkurs dauert drei Wochen, wobei der persönliche Unterricht am Montag und Freitag zur Erörterung von Themen dient, die dienstags bis donnerstags online per Video präsentiert werden.

Teilnehmerinnen und Teilnehmer, die das Programm abschließen, werden in unsere Datenbank der grünen Techniker (m/w/d) aufgenommen. Die Datenbank steht unseren Konsortialpartnern, die Arbeitnehmer in Vollzeit suchen, zur Verfügung.