---
title: Solar Monteur (m/w/d)
slug: solar-monteur
description_short: Nach dem dreiwöchigen Grundkurs beginnen die Teilnehmerinnen
  und Teilnehmer mit einer 160-stündigen Qualifikation. Diese umfasst 80 Stunden
  PV-Installationstheorie, gefolgt von 80 Stunden "Elektrofachkraft für
  festgelegte Tätigkeiten" (Efft). Sie werden befähigt, wiederkehrende
  Installations- und Wartungsarbeiten nach den Vorgaben ihres Arbeitgebers
  selbstständig durchzuführen. Erfahrung mit Handarbeit bevorzugt. CEF-B2.1
  Minimum.
src: media/images/solar-monteur.jpg
subtitle: Solar Monteure (m/w/d) können regelmäßige Aufgaben übernehmen, die für
  die grüne Energiewende unerlässlich sind
duration: Grundkurs + 4 Wochen
program:
  - identifier: "1"
    title: Orientierung im deutschen Arbeitsmarkt (Grundkurs Block 1)
    body: "In der ersten Woche befassen wir uns mit den Erwartungen und Pflichten
      von Arbeitnehmenden gegenüber ihren Arbeitgebern und den Pflichten des
      Arbeitgebers gegenüber den Arbeitnehmenden. Außerdem befassen wir uns mit
      deutschen Industrienormen und Qualifikationsstandards sowie mit
      Arbeitsschutz, Arbeitsrecht, Unfallversicherung, Steuern und der
      wirtschaftlichen Bedeutung des Mittelstandes. "
  - identifier: "2"
    title: Interkulturelle Kompetenz für grüne Techniker (Grundkurs Block 2)
    body: In der zweiten Woche beschäftigen wir uns mit den unausgesprochenen Regeln
      am Arbeitsplatz, dem Verhalten gegenüber Kundinnen und Kunden, Kolleginnen
      und Kollegen, dem Führungsstil und dem Umgang mit Konflikten. Wir befassen
      uns auch mit der Bedeutung von Datensicherheit und die UN SDGs und
      Emissionsziele, die für die grüne Energiebranche relevant sind.
  - identifier: "3"
    title: Energiewende in der Fachrichtung Solar (Grundkurs Block 3)
    body: In der dritten Woche geht es um Sonneneinstrahlung, Solarzellen und den
      Berliner Solarcity-Masterplan. Außerdem diskutieren wir über
      Solarenergie-Lieferketten und Netzanbindung in Deutschland,
      Sektorenkopplung und die Umweltauswirkungen der Solarenergie.
  - identifier: 4 (+4)
    title: Vorbereitung auf den PV-Installationskurs mit anschließender
      160-stündiger praktischer Weiterbildung
    body: Die vierte Woche des Grundkurses dient der Vorbereitung auf das Vokabular
      und die Themen, die in der 4-wöchigen praktischen Ausbildung mit unserem
      Bildungspartner vermittelt werden. Dieser Kurs deckt sowohl die Theorie
      von Photovoltaikanlagen als auch deren Installation ab. Er findet in
      Berlin statt.
  - identifier: Nach Abschluss
    body: Der Qualifizierung zum "EffT" und der Aufnahme in unsere Datenbank der
      grünen Techniker (m/w/d) steht den Konsortialpartnern zur Verfügung, die
      nach Vollzeit-Solarmonteuren (m/w/d) suchen.
is_featured: true
---
In Zusammenarbeit mit Migrantenorganisationen und qualifizierten Bildungspartnern bilden wir Technikerinnen und Techniker mit mindestens B2,1 Deutsch zum Solar Monteur (m/w/d) aus. Nach Abschluss des dreiwöchigen Grundkurses beginnt eine 160-stündige praktische Weiterbildung, die 80 Stunden PV-Anlagentheorie, gefolgt von 80 Stunden "Elektrofachkraft für festgelegte Tätigkeiten" (Efft) umfasst. 

Als qualifizierter Monteur (m/w/d) sind Sie in der Lage, wiederkehrende Installations- und Wartungstätigkeiten nach den Richtlinien eines Arbeitgebers selbständig durchzuführen. Viele unserer Konsortialpartner bieten auch Weiterbildungsmöglichkeiten zur Elektrofachkraft an.

Sie brauchen keine Vorkenntnisse, um den Kurs zu beginnen, aber diejenigen, die eine Ausbildung oder Erfahrung in handwerklichen Tätigkeiten vorweisen können, werden bevorzugt. Montags und freitags finden vomTrainer geleitete Lektionen statt, in denen Themen besprochen werden, die dienstags bis donnerstags online per Videounterricht präsentiert werden. 

Teilnehmerinnen und Teilnehmer, die das Programm abschließen, werden in unsere Datenbank der grünen Techniker (m/w/d) aufgenommen. Die Datenbank steht unseren Konsortialpartnern, die Arbeitnehmende in Vollzeit suchen, zur Verfügung.