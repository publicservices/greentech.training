---
title: International Trainee
slug: international-trainee
text_title: Anmelden
text_form: Bewerben können sich Softwareentwickler, Datensicherheitsexperten und IT-Spezialisten mit mindestens A2.3 Deutsch. Bitte verwenden Sie das Nachrichtenformular für ein kurzes Bewerbungsschreiben, in dem Sie Ihre bisherigen IT-Arbeiten erläutern und nach Möglichkeit einen Link auf ein Repository angeben. Wir werden die für das Trainingsprogramm ausgewählten Teilnehmer innerhalb von 5 Tagen nach der Bewerbung kontaktieren.
newsletter_text: Ja, ich möchte Updates von greentech.training erhalten. Diese Einwilligung kann ich jederzeit widerrufen, indem ich auf den Abmeldelink in E-Mails klicke oder den Widerruf per E-Mail an info@greentech.training sende.
is_featured: false
firstname_key: Vorname
lastname_key: Nachname
email_key: E-Mail
cef_key: CEF-Niveau
nationality_key: Nationalität
location_key: Derzeitiger Standort
message_key: Nachricht
submit_key: Abschicken
---
