---
title: Contact
slug: contact
text_title: Was können wir für Sie tun?
text_form: Haben Sie Fragen zu unseren Dienstleistungen? Bitte schauen Sie in die FAQs. Die meisten Fragen zu Bildungswegen und Konsortialpartnerschaften werden dort beantwortet. Wenn Sie nicht finden können, wonach Sie suchen, benutzen Sie bitte das Formular, um uns Ihre Anfrage zu schicken.
newsletter_text: Ja, ich möchte Updates von greentech.training erhalten. Diese Einwilligung kann ich jederzeit widerrufen, indem ich auf den Abmeldelink in E-Mails klicke oder den Widerruf per E-Mail an info@greentech.training sende.
is_featured: true
firstname_key: Vorname
lastname_key: Nachname
email_key: E-Mail
cef_key: CEF-Niveau
nationality_key: Nationalität
location_key: Derzeitiger Standort
message_key: Nachricht
submit_key: Abschicken
---
