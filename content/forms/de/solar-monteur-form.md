---
title: Solar Monteur
slug: solar-monteur
text_title: Anmelden
text_form: Die Teilnahme am greentech.training-Programm ist kostenlos für diejenigen, die durch ein zweistufiges Anmeldeverfahren ausgewählt werden. Um loszulegen, geben Sie Ihre Daten und ein kurzes Bewerbungsschreiben in das Nachrichtenfeld ein. Diejenigen, die ausgewählt werden, erhalten innerhalb von fünf Tagen nach der Bewerbung einen Link für ein Online-Interview per E-Mail; stellen Sie also sicher, dass Sie Zugang zu einer Kamera und einem Mikrofon haben. Das Interview dient dazu, Ihre Deutschkenntnisse und Ihre Arbeitserfahrung zu ermitteln.
newsletter_text: Ja, ich möchte Updates von greentech.training erhalten. Diese Einwilligung kann ich jederzeit widerrufen, indem ich auf den Abmeldelink in E-Mails klicke oder den Widerruf per E-Mail an info@greentech.training sende.
is_featured: true
firstname_key: Vorname
lastname_key: Nachname
email_key: E-Mail
cef_key: CEF-Niveau
nationality_key: Nationalität
location_key: Derzeitiger Standort
message_key: Nachricht
submit_key: Abschicken
---
