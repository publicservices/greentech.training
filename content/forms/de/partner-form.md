---
title: Partner
slug: join-partner
text_title: Partner werden
text_form: Um Ihr Interesse an einer Konsortialmitgliedschaft zu bekunden, füllen Sie bitte die nachstehenden Felder aus. Bitte verwenden Sie bei Ihrer Bewerbung Ihre geschäftliche E-Mail-Adresse. Wir werden uns innerhalb von 2 Werktagen mit Ihnen in Verbindung setzen und Ihnen weitere Einzelheiten mitteilen.
newsletter_text: Ja, ich möchte Updates von greentech.training erhalten. Diese Einwilligung kann ich jederzeit widerrufen, indem ich auf den Abmeldelink in E-Mails klicke oder den Widerruf per E-Mail an info@greentech.training sende.
is_featured: true
firstname_key: Vorname
lastname_key: Nachname
email_key: E-Mail
cef_key: unused
nationality_key: unused
location_key: unused
message_key: Nachricht
submit_key: Abschicken
---
