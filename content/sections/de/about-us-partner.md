---
slug: about-us-partner
blocks:
  - type: content
    content: Greentech.training arbeitet nach einem sozialen Optimierungsmodell. Das
      bedeutet, dass die Anfangsfinanzierung unserer Arbeit aus der Gemeinschaft
      stammt. Am Ende des Jahres schütten wir einen Teil des erwirtschafteten
      Gewinns an die Partner in unserem Netzwerk, Migrantenorganisationen und
      Integrationsschulen aus, um deren Arbeit zu unterstützen. Auf diese Weise
      unterstützen unsere Ökostrompartner indirekt die stärkere Integration
      künftiger grüner Techniker (m/w/d) in die Gesellschaft und tragen
      gleichzeitig dazu bei, den ökologischen Fußabdruck der Menschheit auf
      unserem Planeten zu verringern. Deshalb nennen wir es sozial integrierte
      Nachhaltigkeit.
  - type: image
    title: Partner werden
    src: media/images/iceburg.jpeg
  - type: cta
    title: Partner werden
    link: join/partner
---
