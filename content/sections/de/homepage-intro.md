---
slug: homepage-intro
blocks:
  - type: title
    content: Grüne Techniker (m/w/d) für die Energiewende
  - type: content
    content: Die Plattform für eine saubere, kühlere Erde.
  - type: cta
    title: Mehr erfahren
    link: about
  - type: image
    src: media/images/homepage_intro_forest.jpeg
    title: " greentech.training, grüne Techniker (m/w/d) für die Energiewende"
---
