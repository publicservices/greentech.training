---
slug: join-international-trainee-intro
blocks:
  - type: title
    content: Auf der Suche nach einem Auslandspraktikum?
  - type: content
    content: Starten Sie Ihre Karriere als Motor der deutschen Energiewende
  - type: image
    title: Join International Trainee
    src: media/images/join-international-trainee-intro-v2.jpg
---
