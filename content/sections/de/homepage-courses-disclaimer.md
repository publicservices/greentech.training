---
slug: homepage-courses-disclaimer
blocks:
  - type: content
    content: Kurse werden voraussichtlich Anfang 2023 beginnen. Melden Sie Ihr
      Interesse an einem Programm frühzeitig über das Kontaktformular oder
      direkt über die Anmeldeformulare auf den jeweiligen Kursseiten an.
---
