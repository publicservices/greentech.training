---
slug: join-trainee-intro
blocks:
  - type: title
    content: Ihr Weg in eine grünere Zukunft
  - type: content
    content: Bewerben Sie sich als grüner Techniker (m/w/d) um mit unserem Netzwerk
      von Partnern am Tag Ihres Abschlusses verbunden zu werden
  - type: image
    title: Für Training bewerben
    src: media/images/trainee-intro-v2.jpg
---
