---
slug: about-us-concept
blocks:
  - type: title
    content: Gemeinschaftlich integrierte Nachhaltigkeit
  - type: content
    content: Deutschland leidet unter einem Mangel an angelernten und qualifizierten
      Arbeitskräften. Es ist auch ein Land, das zur neuen Heimat für Menschen
      geworden ist, die durch Krieg, Klima und wirtschaftliche Not vertrieben
      wurden. Mit greentech.training wollen wir diese beiden Welten
      zusammenbringen, um die Kohlenstoffemissionen in unserer gemeinsamen Welt
      zu reduzieren. Deshalb haben wir gemeinsam mit Partnern aus
      Migrantenorganisationen, Bildungseinrichtungen und der grünen
      Energiebranche ein innovatives Ausbildungs- und Rekrutierungskonzept
      entwickelt, das Migrantinnen und Migranten, insbesondere Geflüchtete in
      der grünen Energiebranche qualifiziert und vermittelt. Ab Ende 2023 werden
      wir die Plattform für die internationale Gemeinschaft öffnen, um grüne
      Techniker (m/w/d) auszubilden, die an der Schnittstelle von Nahrung,
      Energie und Wasser arbeiten.
  - type: image
    title: Gemeinschaftlich Integrierte Nachhaltigkeit
    src: media/images/about_us_forest.jpeg
---
