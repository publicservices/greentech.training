---
slug: faqs-intro
blocks:
  - type: title
    content: Brauchen Sie einen Wegweiser?
  - type: content
    content: Hier finden Sie einige der häufig gestellten Fragen zu unserem Kurs und
      Konzept.
  - type: image
    title: FAQs
    src: media/images/faqs-intro-v2.jpg
---
