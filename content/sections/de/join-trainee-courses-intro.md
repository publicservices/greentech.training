---
slug: join-trainee-courses-intro
blocks:
  - type: title
    content: Drei Wege zur sofortigen Beschäftigung für ein cooleres Morgen
  - type: content
    content: "Unser einzigartiges Schulungskonzept wurde entwickelt, um die
      Integration ausländischer Arbeitnehmender in die deutsche
      Ökoenergie-Branche zu unterstützen. Der Kurs besteht aus einem
      dreiwöchigen Grundkurs in deutschen Arbeits- und Kulturnormen, gefolgt von
      einer Woche Einblick in die Solarindustrie. In der vierten Woche werden
      Sie in eine von drei partner-definierten Laufbahnen eingeteilt:"
---
