---
slug: course-solar-specialist-intro
blocks:
  - type: title
    content: Lenken Sie Ihre Karriere in Richtung 1,5°!
  - type: content
    content: Unsere Ökostrom-Partner arbeiten an der Dekarbonisierung des Planeten.
      Mach mit!
  - type: image
    title: Solar Assistant (m/w/d)
    src: media/images/course-solar-specialist-intro-v2.jpg
---
