---
slug: course-solar-assistant-intro
blocks:
  - type: title
    content: Sei die Veränderung, die Du in der Energiewende sehen willst!
  - type: content
    content: Solar Assistants (m/w/d) treiben die Energiewende in Berlin an
  - type: image
    title: Solar Assistant (m/w/d)
    src: media/images/course-solar-assistant-intro-v2.jpg
---
