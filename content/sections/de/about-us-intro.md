---
slug: about-us-intro
blocks:
  - type: title
    content: Wer ist greentech.training?
  - type: content
    content: Ein Team von IT- und Schulungsspezialistinnen - und spezialisten, das
      sich dafür einsetzt, dass Ökoenergieunternehmen ihre Emissionsziele
      erreichen und den CO₂-Fußabdruck der Menschheit verringern.
  - type: image
    title: Über uns
    src: media/images/über-uns-intro.jpeg
---
