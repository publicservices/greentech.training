---
slug: homepage-concept
blocks:
  - type: title
    content: Konzipiert für das Berliner Solargesetz 2023
  - type: content
    content: Eine kühlere Erde ist möglich. Dies erfordert jedoch länder- und
      sektorübergreifende Programme, die allen zugute kommen. Bei
      greentech.training bauen wir ein solches Programm auf. Unsere Community
      Integrated Sustainability Platform wird in Berlin entwickelt, um
      Migrantinnen und Migranten, insbesondere Geflüchtete weiterzubilden und
      sie direkt mit unseren Partnerunternehmen zu verbinden, die sich für eine
      kühlere Erde einsetzen. Die Bewerberinnen und Bewerber für das Programm
      werden nach ihren Deutschkenntnissen (mindestens B1.2) und ihrer
      Berufserfahrung befragt. Diejenigen, die bereits über Qualifikationen
      verfügen, werden in unser Solarspezialistenprogramm aufgenommen, das mit
      einem Job Shadowing bei unseren Partnern abschließt. Teilnehmerinnen und
      Teilnehmer mit wenig oder gar keiner Berufserfahrung werden ab 2023 in
      unsere Programme Solar Assistant (EuP) (m/w/d) oder Solarmonteur (EffT)
      (m/w/d) aufgenommen. Sobald die Teilnehmerinnen und Teilnehmer ihre
      Ausbildung abgeschlossen haben, werden sie in unsere Datenbank für grüne
      Techniker (m/w/d) aufgenommen und können sofort eingestellt werden.
  - type: image
    src: media/images/viktoriastatue_siegessaeule_berlin.png
    title: Greentech.berlin concept
---
