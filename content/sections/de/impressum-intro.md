---
slug: impressum-intro
blocks:
  - type: title
    content: Impressum
  - type: content
    content: |-
      Diese Internetpräsenz ist ein Informationsangebot von greentech.training
  - type: image
    title: Impressum
    src: media/images/impressum-intro-v2.jpg
---
