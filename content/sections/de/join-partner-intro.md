---
slug: join-partner-intro
blocks:
  - type: title
    content: Die nächste Generation von grünen Technikern (m/w/d) integrieren
  - type: content
    content: Jetzt Konsortialpartner von greentech.training werden
  - type: image
    title: Partner werden
    src: media/images/partner-intro-v2.jpg
---
