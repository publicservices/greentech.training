---
slug: course-solar-monteur-intro
blocks:
  - type: title
    content: Unsere Erde hat noch viel zu bieten!
  - type: content
    content: Den Planeten kühl halten und sich einen sicheren Arbeitsplatz als Solar
      Monteur (m/w/d) sichern
  - type: image
    title: Solar Monteur (m/w/d)
    src: media/images/course-solar-monteur-intro-v2.jpg
---
