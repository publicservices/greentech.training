---
title: Partner
slug: partner
subtitle: Werden Sie Mitglied des wachsenden Konsortiums von Unternehmen, die
  der Energiewende und der Gesellschaft dienen
offer:
  title: Unser Leistungsversprechen
  blocks:
    - title: Vertrauen
      body: Unsere Technikerinnen und Techniker sind nicht nur Absolventen eines
        Programms, sie bleiben geschätzte Mitglieder der
        greentech.training-Familie. Aufgrund unseres "Free for
        Technicians"-Rekrutierungsmodells wählen wir diejenigen aus, von denen
        wir glauben, dass sie die Werte der Ehrlichkeit, des Kundendienstes und
        des Vertrauens respektieren.
    - title: Tiefe Integration
      body: Die Absolventinnen und Absolventen unseres Programms bleiben bis zu drei
        Jahre nach Abschluss des Kurses Teil unserer Plattform und können sich
        mit Fragen und Anliegen im Zusammenhang mit ihrer Arbeit an uns wenden.
        Auf diese Weise bleiben wir ein starker Partner, der sich für die
        langfristige Vermittlung von Fachkräften und die Verringerung der
        Abwanderung einsetzt.
    - title: Mediation
      body: Für unsere Konsortialpartner bieten wir Integrationsworkshops sowie
        Mediationsdienste zwischen Partnern und neu integrierten Technikerinnen
        und Technikern an. Dieser Ansatz trägt dazu bei, die Zuverlässigkeit,
        den Einfallsreichtum und das Engagement der Mitarbeitenden in
        Unternehmen zu fördern, die ihre Mitarbeitenden offen schätzen.
---
Deutschlands erste sozial integrierte Nachhaltigkeitsplattform bereitet grüne Fachkräfte auf das Jahr 2023 und darüber hinaus vor. Wir laden alle nachhaltigen Energieunternehmen ein, sich uns anzuschließen.

* Sparen Sie Personalkosten durch den direkten und unkomplizierten Zugriff auf unseren Pool an frisch ausgebildeten Solar Assistants (m/w/d) und Solar Monteuren (m/w/d).
* Nehmen Sie Kontakt zu unserem geprüften Pool an deutschsprachigen internationalen Bewerberinnen und Bewerbern auf, die auf IT, Datensicherheit und andere für die Energiewende notwendige Bereiche spezialisiert sind.
* Nutzen Sie den Win-Win-Win-Ansatz von greentech.training, indem Sie Geflüchtete integrieren, die bereit sind, die Energiewende in Deutschland für eine saubere Zukunft mitzugestalten.
* Greifen Sie auf unseren erfahrenen, mehrsprachigen Pool von grünen Technikern (m/w/d) zu, die sofort eingestellt werden können.
* Die Mitgliedschaft im Konsortium 2023 ist kostenlos.
* Profitieren Sie von einem vertrauenswürdigen Partner an Ihrer Seite, der Sie bei der Arbeitserlaubnis, der Vermittlung von Sprachkursen und langfristigen Integrationsleistungen unterstützt.