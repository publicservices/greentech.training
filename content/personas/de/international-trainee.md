---
title: "International Trainee "
slug: international-trainee
subtitle: Mit unserem Screening-, Schulungs- und Rekrutierungsprozess einer
  Festanstellung in Deutschland einen Schritt näher kommen
offer:
  title: ""
---
Deutschland befindet sich mitten in einer grünen Energiewende und hat sich das ehrgeizige Ziel gesetzt, bis 2045 klimaneutral zu werden. Bis 2030 sollen die Emissionen im Vergleich zu 1990 um mindestens 65 Prozent gesenkt werden. Um dies zu erreichen, werden qualifizierte IT- und Datensicherheitsexpertinnen und -experten aus aller Welt benötigt.

greentech.training richtet sich an frischgebackene Informatik-Masterabsolventinnen und -absolventen sowie Berufserfahrene, um zunächst einen Einblick in die deutsche Arbeitskultur und deren Standards zu erhalten, um zu zeigen, wie ihre Fähigkeiten Unternehmen im Bereich der erneuerbaren Energien unterstützen können, und um sie schließlich mit diesen Unternehmen für Remote-Praktika zusammenzubringen.

Wir nehmen Bewerbungen für einen Start Anfang 2023 entgegen. Melden Sie Ihr Interesse an einem Kurs an, indem Sie das untenstehende Formular ausfüllen.

Folgendes werden Sie während unseres 4-wöchigen Kurses, der 8 Tage Online-Unterricht mit unserem Kurstrainer umfasst, lernen (beachten Sie, dass sich die Einzelheiten des Kurses vor Kursbeginn ändern können):