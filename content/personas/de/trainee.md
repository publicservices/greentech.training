---
title: Trainee
slug: trainee
subtitle: So werden Sie zum grünen Techniker (m/w/d)
offer:
  title: Mehr als ein Trainingspartner
  blocks:
    - title: Schnelle Beschäftigung
      body: Unser Ansatz besteht darin, in jedem Kurs eine ausgewählte Gruppe von
        zuverlässigen Technikerinnen und Technikern vorzubereiten, die sofort
        eine Vollzeitbeschäftigung bei unseren Partnern aufnehmen können.
        Diejenigen, die bereits Erfahrung haben, haben die Möglichkeit, sofort
        bei unseren Konsortialpartnern anzufangen.
    - title: Zugang zur Plattform
      body: Sie bleiben bis zu drei Jahre nach Abschluss Ihres Kurses ein willkommener
        Teil unserer Plattform. In dieser Zeit wenden Sie sich mit allen Fragen
        und Anliegen, die Ihre Mitarbeitenden betreffen, an uns. Auf diese Weise
        wollen wir ein hilfreicher Partner bleiben, der sich für Ihre
        langfristige Zufriedenheit und Ihren Erfolg am Arbeitsplatz einsetzt.
    - title: Hilfe beim Wohnortwechsel
      body: Wenn Sie in eine andere Stadt ziehen müssen, um bei einem Mitglied unseres
        Konsortiums zu arbeiten, können wir Sie mit der örtlichen
        Migrantenorganisation in Verbindung bringen, die für Kinderbetreuung,
        sportliche Aktivitäten, Wohnungshilfe und andere Dienstleistungen
        zuständig ist, um Ihren Übergang zu unterstützen.
---
Bewerbungen aus Berlin und Brandenburg nehmen wir ab Oktober 2022 für einen geplanten Kursbeginn Anfang 2023 entgegen.

Um Ihr Interesse an einem greentech.training-Pathway zu bekunden, wählen Sie eine der oben genannten Karten auf der Grundlage Ihrer derzeitigen Erfahrung und Ihres Deutschniveaus und füllen Sie das Bewerbungsformular aus. Ausführliche Informationen zu den einzelnen Ausbildungsgängen und dem Programm selbst finden Sie im Bereich FAQ. Nutzen Sie das Nachrichtenfeld des Formulars, um einen kurzen Überblick über Ihre Ausbildung und Ihren Hintergrund zu geben und zu erklären, warum Sie an dem Kurs teilnehmen möchten. Wir werden Ihnen innerhalb weniger Arbeitstage eine Einladung zu einem Online-Interview zusenden, wenn wir Sie für das Programm für geeignet halten.

Wenn Sie weitere Fragen haben, nutzen Sie bitte das Kontaktformular, um uns eine Nachricht zu senden.