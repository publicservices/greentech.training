---
title: Impressum
slug: impressum
sections:
  - impressum-intro
---
Kombinationsimpressum (Anbieterkennzeichnung) aus Telemediengesetz (TMG), Staatsvertrag für Rundfunk und Telemedien (RStV) und Informationen gemäß Dienstleistungs-Informationspflichten-Verordnung (DL-InfoV) basierend auf der EU-Dienstleistungsrichtlinie (RL 2006/123/EG Amtsblatt der EU2006, L376/36 „DRLR“).

Anbieter und somit verantwortlich für die kommerzielle und geschäftsmäßige Website im Sinne des § 5 des Telemediengesetz in Funktion als Kerngesetz des Elektronischer-Geschäftsverkehr-Vereinheitlichungsgesetz ElGVG und des Gesetz zur Regelung der Rahmenbedingungen für Informations- und Kommunikationsdienste (IuKDG), ist die greentech.training, vertreten durch Jerome Goerke.

Informationen gemäß § 2 Absatz 1 Dienstleistungs-Informationspflichten-Verordnung (DL-InfoV):

**GREENTECH.berlin e.V. (i.Gr.) als Träger der greentech.training**

**Anschrift:** Tempelhofer Ufer 36, 10963 Berlin.

**E-mail:** info@greentech.training 

**Internet:** [](https://sonnen.de/)greentech.training

**Plattform der EU-Kommission zur Online-Streitbeilegung:** [www.ec.europa.eu/consumers/odr](https://www.ec.europa.eu/consumers/odr)

**Anwendbares Recht:** Recht der Bundesrepublik Deutschland (BRD) 

**Sitz der Gesellschaft:** Berlin

**Amtsgericht:** Berlin Kreuzberg 

**Finanzamt:** Berlin Kreuzberg

**Bilder:** freepik.com und rawpixel.com.

Homepage-Header-Bild: CC0 Public Domain

Homepage Statue Bild: CC0 Public Domain

Homepage Kurs Bild 1: Bild von senivpetro auf Freepik

Homepage Kurs Bild 2: Bild von prostooleh auf Freepik

Homepage Kurs Bild 3: Bild von senivpetro auf Freepik

Über uns: Header Image: CC0 Public Domain

Über uns: Waldbild von eberhard grossgasteiger für rawpixel.com

Über uns: Polar Bild von NASA/Saskia Madlener CC0 Public Domain

Internationaler Bewerber: Bild von artursafronovvvv auf Freepik

Partner Werden: Image by bluejayphoto/iStock

Sich für Training bewerben: Bild von sborisov

Übrige Bilder unter CC0 Public Domain.