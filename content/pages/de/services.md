---
title: Gemeinschaftlich integrierte Nachhaltigkeit
---
Unser Prozess und Schulungsansatz sind so konzipiert, dass sie die Unsicherheit bei der Einstellung sowohl für die Teilnehmer als auch für die Energiepartnerunternehmen weitgehend verringern. Zu diesem Zweck bieten wir einen Ansatz an, der aus drei Dienstleistungen besteht: Schulung, Einstellung und Begleitung. Alle drei sind auf der greentech.training-Plattform angesiedelt.
