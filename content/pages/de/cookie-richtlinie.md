---
title: Cookie Richtlinie
slug: cookie-richtlinie
---
## **Allgemeine Hinweise**

Unsere Internetseiten verwenden Cookies. Cookies sind kleine Textdateien und richten auf Ihrem Endgerät keinen Schaden an. Sie werden entweder vorübergehend für die Dauer einer Sitzung (Session-Cookies) oder dauerhaft (permanente Cookies) auf Ihrem Endgerät gespeichert. Session-Cookies werden nach Ende Ihres Besuchs automatisch gelöscht. Permanente Cookies bleiben auf Ihrem Endgerät gespeichert, bis Sie diese selbst löschen oder eine automatische Löschung durch Ihren Webbrowser erfolgt.

Cookies haben verschiedene Funktionen. Zahlreiche Cookies sind technisch notwendig, da bestimmte Websitefunktionen ohne diese nicht funktionieren würden (z. B. die Warenkorbfunktion oder die Anzeige von Videos). Andere Cookies dienen dazu, das Nutzerverhalten auszuwerten oder Werbung anzuzeigen.

Cookies, die zur Durchführung des elektronischen Kommunikationsvorgangs, zur Bereitstellung bestimmter, von Ihnen erwünschter Funktionen (z. B. für eine Warenkorbfunktion) oder zur Optimierung der Website (z. B. Cookies zur Messung des Webpublikums) erforderlich sind (notwendige Cookies), werden auf Grundlage von Art. 6 Abs. 1 lit. f DSGVO gespeichert, sofern keine andere Rechtsgrundlage angegeben wird. Der Websitebetreiber hat ein berechtigtes Interesse an der Speicherung von notwendigen Cookies zur technisch fehlerfreien und optimierten Bereitstellung seiner Dienste. Sofern eine Einwilligung zur Speicherung von Cookies und vergleichbaren Wiedererkennungstechnologien abgefragt wurde, erfolgt die Verarbeitung ausschließlich auf Grundlage dieser Einwilligung (Art. 6 Abs. 1 lit. a DSGVO und § 25 Abs. 1 TTDSG); die Einwilligung ist jederzeit widerrufbar.

Sie können Ihren Browser so einstellen, dass Sie über das Setzen von Cookies informiert werden und Cookies nur im Einzelfall erlauben, die Annahme von Cookies für bestimmte Fälle oder generell ausschließen sowie das automatische Löschen der Cookies beim Schließen des Browsers aktivieren. Bei der Deaktivierung von Cookies kann die Funktionalität dieser Website eingeschränkt sein.

Teilweise können auch Cookies von Drittunternehmen auf Ihrem Endgerät gespeichert werden, wenn Sie unsere Seite betreten (Third-Party-Cookies). Diese ermöglichen uns oder Ihnen die Nutzung bestimmter Dienstleistungen des Drittunternehmens (z. B. Cookies zur Abwicklung von Zahlungsdienstleistungen). Soweit Cookies von Drittunternehmen oder zu Analysezwecken eingesetzt werden, werden wir Sie hierüber im Rahmen dieser Datenschutzerklärung gesondert informieren und ggf. eine Einwilligung abfragen. Wir verwenden die folgenden Cookies von Drittanbietern:

1. Der LinkedIn Insight-Tag. Dies ermöglicht es uns, Kampagnenberichte zu erstellen und wertvolle Erkenntnisse über Website-Besucher zu gewinnen, die über die von uns auf LinkedIn durchgeführten Kampagnen kommen.

**Wie kann ich Cookies ablehnen und löschen?**

Sie können sich entscheiden, alle oder bestimmte Arten von durch Ihren Besuch eingestellten Cookies abzulehnen oder zu blockieren, indem Sie auf die Cookie-Einstellungen auf unserer Webseite klicken. Bitte beachten Sie, dass die meisten Browser Cookies automatisch akzeptieren. Sollten Sie nicht wollen, dass Cookies verwendet werden, kann es sein, dass Sie die Cookies aktiv löschen oder blockieren müssen. Nach dem Ablehnen der Verwendung von Cookies sind Sie zwar in der Lage, unsere Webseiten zu besuchen, einige Funktionen jedoch möglicherweise nicht richtig funktionieren. Weitere Einzelheiten über das Löschen und Ablehnen von Cookies und allgemeine Informationen über Cookies finden Sie außerdem auf <https://www.aboutcookies.org>. Mit der Nutzung unserer Webseite, ohne dabei einige oder alle Cookies zu löschen oder abzulehnen, erklären Sie sich damit einverstanden, dass wir diese Cookies platzieren können, die Sie auf Ihrem Gerät nicht gelöscht oder abgelehnt haben.