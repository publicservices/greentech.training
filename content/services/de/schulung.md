---
title: Schulung
slug: schulung
name: schulung
index: 1
steps:
  - name: |
      Woche 1.
      Arbeitsmarkt
  - name: |
      Woche 2.
      Arbeitskultur
  - name: |
      Woche 3.
      Solarindustrie
  - name: |
      Woche 4 (+4).
      Abschluss
---
Der Schulungsservice bietet Migrantinnen und Migranten Einblicke in die deutsche Arbeitskultur und Erwartungen, gefolgt von einem Einblick in den grünen Energiesektor selbst. Der Kurs schließt mit einem zertifizierten Training, einer Projektarbeit oder einem Job Shadowing ab, um die in Deutschland ansässigen Teilnehmerinnen und Teilnehmer mit den Arbeitsmethoden und Standards der Solarbranche vertraut zu machen. Internationale Bewerberinnen und Bewerber mit IT-Erfahrung werden in unsere Datenbank für Remote-Praktika bei unserem Konsortium aus deutschen Ökostrom-Unternehmen aufgenommen.