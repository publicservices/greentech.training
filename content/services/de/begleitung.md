---
title: Begleitung
slug: begleitung
index: 3
steps:
  - name: Unterstützung beim Umzug
  - name: Aufenthalt Anfragen
  - name: Mediation
  - name: Integration Workshops
---
Die in der Begleitphase erbrachten Leistungen sollen die langfristige Integration der neuen Arbeitskräfte in ihre Unternehmen sicherstellen, die Fluktuation verringern und die Zufriedenheit von Arbeitnehmenden und Arbeitgebern erhöhen. Das Programm kann somit als Systematisierungs- und Beschäftigungsbrücke für nicht einheimische Personen mit ungeregelten Berufen gesehen werden, die eine langfristige Perspektive auf dem deutschen Ökoenergiemarkt suchen.