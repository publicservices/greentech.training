---
title: Einstellung
slug: einstellung
index: 2
steps:
  - name: |
      Anerkennung Unterstützung
  - name: Hospitation
  - name: Datenbank grüne Techniker (m/w/d)
  - name: Arbeitgeber Konsortium
---
Der Einstellungsbereich der Plattform ist für Ökoenergieunternehmen zugänglich, die qualifizierte, angelernte und ungelernte Arbeitskräfte mit einem Mindestniveau von B1 suchen. Hier finden Partnerunternehmen aktuelle Informationen darüber, welche Arbeitsplätze für Migrantinnen und Migranten, insbesondere Geflüchtete, die das Programm absolvieren, rechtlich zulässig sind, sowie Antworten auf Fragen zum Thema Aufenthaltserlaubnis. Diese Informationen sind mit der HWK, dem BAMF und den Innungen abgestimmt.