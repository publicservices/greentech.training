---
name: footer
links:
  - title: Über uns
    url: about
  - title: FAQs
    url: faqs
  - title: Impressum
    url: impressum
  - title: Datenschutz
    url: datenschutz
  - title: Cookie Richtlinie
    url: cookie-richtlinie
  - title: Kontakt
    url: contact
  - title: Admin
    url: admin/index.html
---
