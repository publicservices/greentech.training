---
name: site
links:
  - title: Internationale Bewerberinnen und Bewerber
    url: join/international-trainee
  - title: Sich für Training bewerben
    url: join/trainee
  - title: Konsortium Partner werden
    url: join/partner
---
