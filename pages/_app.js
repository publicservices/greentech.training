import "@/styles/globals.css";
import "@fontsource/livvic";
import "@fontsource/livvic/500.css";
import "@fontsource/catamaran";
import "@fontsource/catamaran/500.css";
import Head from "next/head";
import favicon from "../public/favicon.ico";

function MyApp({ Component, pageProps }) {
	return (
		<>
			<Head>
				<link rel="shortcut icon " href={favicon.src} />
			</Head>
			<Component {...pageProps} />
		</>
	);
}

export default MyApp;
