import Layout from '@/components/layout'
import Section from '@/components/section'
import Slider from '@/components/services/slider'

import pageStyles from '@/styles/pages/services.module.css'

import {
	getMenu,
	getServices,
	getPage,
	getPageSections
} from '@/utils/api.js'

export default function CoursesPage({
	siteMenu,
	footerMenu,
	socialMenu,
	services,
	page,
	sections
}) {
	return (
		<Layout siteMenu={siteMenu} footerMenu={footerMenu} socialMenu={socialMenu}>
			<Section>
				<header className={pageStyles.header}>
					<h1 className={pageStyles.title}>
						{page.frontmatter.title}
					</h1>
					<div className={pageStyles.content}>
						{page.content}
					</div>
				</header>
			</Section>
			<Section>
				{services.length ? (
					<Slider
						services={services}
						active={true}
					/>
				) : null}
			</Section>
		</Layout>
	)
}

export const getStaticProps = async (props) => {
	const siteMenu = await getMenu('site', props)
	const footerMenu = await getMenu('footer', props)
	const socialMenu = await getMenu('social', props)
	const services = await getServices(props)
	const page = await getPage('services', props)
	const sections = await getPageSections('services', props)
	return {
		props: {
			siteMenu,
			footerMenu,
			socialMenu,
			services,
			page,
			sections,
		}
	}
}
