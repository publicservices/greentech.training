import Layout from '@/components/layout'
import Section from '@/components/section'
import SectionIntro from '@/components/sections/section-intro'

import styles from '@/styles/pages/faqs.module.css'

import {
	getMenu,
	getFaqs,
	getPage,
	getPageSections
} from '@/utils/api.js'

const Card = ({faq}) => {
	const {
		title,
		questions,
	} = faq.frontmatter
	return (
		<>
			<header className={styles.faqCard}>
				<h2 className={styles.faqTitle}>
					{title}
				</h2>
			</header>
			{questions ? (
				<main>
					<ul className={styles.listQuestions}>
						{
							questions.map((question, index) => {
								return (
									<li key={index}>
										<article className={styles.questionCard}>
											<details>
												<summary className={styles.questionCardTitle}>
													{question.title}
												</summary>
												<main className={styles.questionCardMain}>
													{question.body}
												</main>
											</details>
										</article>
									</li>
								)
							})
						}
					</ul>
				</main>
			) : null}
		</>
	)
}

const List = ({faqs}) => {
	return (
		faqs.map((faq, index) => {
			return (
				<article key={index} className={styles.listFaqs}>
					<Card faq={faq}/>
				</article>
			)
		})
	)
}

export default function FaqsPage({
	siteMenu,
	footerMenu,
	socialMenu,
	page,
	sections,
	faqs,
}) {
	const intro = sections.find(s => s.slug === 'faqs-intro')
	return (
		<Layout siteMenu={siteMenu} footerMenu={footerMenu} socialMenu={socialMenu}>
			<Section>
				<SectionIntro blocks={intro.frontmatter.blocks}/>
				<main className={styles.page}>
					{faqs?.length ? (
						<List faqs={faqs}/>
					) : null}
				</main>
			</Section>
		</Layout>
	)
}

export const getStaticProps = async (props) => {
	const siteMenu = await getMenu('site', props)
	const footerMenu = await getMenu('footer', props)
	const socialMenu = await getMenu('social', props)
	const faqs = await getFaqs(props)
	const page = await getPage('faqs', props)
	const sections = await getPageSections('faqs', props)
	return {
		props: {
			siteMenu,
			footerMenu,
			socialMenu,
			faqs,
			page,
			sections,
		}
	}
}
