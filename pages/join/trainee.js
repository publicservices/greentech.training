import pageStyles from '@/styles/pages/join.module.css'
import Layout from '@/components/layout'
import Section from '@/components/section'
import SectionIntro from '@/components/sections/section-intro'
import SectionConcept from '@/components/sections/section-concept'
import SectionJoinConcept from '@/components/sections/join-concept'
import SectionJoinOffer from '@/components/sections/join-offer'
import ListCourses from '@/components/courses/list'

import {
	getMenu,
	getPersonae,
	getCourses,
	getPageSections,
} from '@/utils/api.js'

export default function TraineePage({
	siteMenu,
	footerMenu,
	socialMenu,
	sections,
	personae,
	courses,
}) {
	const intro = sections.find(s => s.slug === 'join-trainee-intro')
	const introCourses = sections.find(s => s.slug === 'join-trainee-courses-intro')
	const {contentHtml} = personae
	const {
		title,
		subtitle,
		offer,
	} = personae.frontmatter

	return (
		<Layout
			siteMenu={siteMenu}
			footerMenu={footerMenu}
			socialMenu={socialMenu}
		>
			<Section>
				<SectionIntro blocks={intro.frontmatter.blocks}/>
				{courses.length ? (
					<Section
						fullHeight
					>
						{/*<SectionConcept blocks={introCourses.frontmatter.blocks}/>*/}
						<ListCourses courses={courses}/>
					</Section>
				) : null}
				<SectionJoinConcept title={subtitle} content={contentHtml}/>
				<SectionJoinOffer offer={offer}/>
			</Section>
		</Layout>
	)
}

export const getStaticProps = async (props) => {
	const siteMenu = await getMenu('site', props)
	const footerMenu = await getMenu('footer', props)
	const socialMenu = await getMenu('social', props)

	const sections = await getPageSections('join-trainee', props)

	const personae = await getPersonae('trainee', props)
	const courses = await getCourses(props)
	return {
		props: {
			siteMenu,
			footerMenu,
			socialMenu,
			sections,
			personae,
			courses: courses.filter(c => c.frontmatter.is_featured === true),
		}
	}
}
