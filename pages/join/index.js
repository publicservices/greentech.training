import Layout from '@/components/layout'
import Section from '@/components/section'
import List from '@/components/personas/list'

import pageStyles from '@/styles/pages/join.module.css'

import {
	getMenu,
	getPage,
	getPageSections,
	getPersonas,
} from '@/utils/api.js'

export default function JoinPage({
	siteMenu,
	footerMenu,
	socialMenu,
	personas,
	page,
	sections
}) {
	return (
		<Layout
			siteMenu={siteMenu}
			footerMenu={footerMenu}
			socialMenu={socialMenu}
		>
			<Section fullHeight transparent>
				<div className={pageStyles.container}>
					<header>
						<h1 className={pageStyles.title}>{page.frontmatter.title}</h1>
					</header>
					{personas.length && <List personas={personas}></List>}
				</div>
			</Section>
		</Layout>
	)
}

export const getStaticProps = async (props) => {
	const siteMenu = await getMenu('site', props)
	const footerMenu = await getMenu('footer', props)
	const socialMenu = await getMenu('social', props)

	const personas = await getPersonas(props)
	const page = await getPage('join', props)
	const sections = await getPageSections('join', props)
	return {
		props: {
			siteMenu,
			footerMenu,
			socialMenu,
			personas,
			page,
			sections,
		}
	}
}
