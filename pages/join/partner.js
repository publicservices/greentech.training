import pageStyles from "@/styles/pages/join.module.css";
import Layout from "@/components/layout";
import Section from "@/components/section";
import SectionIntro from "@/components/sections/section-intro";
import SectionJoinConcept from "@/components/sections/join-concept";
import SectionJoinOffer from "@/components/sections/join-offer";
import Form from '@/components/forms/form'
import { getMenu, getPersonae, getPageSections, getForm } from "@/utils/api.js";

export default function PartnerPage({
	siteMenu,
	footerMenu,
	socialMenu,
	personae,
	sections,
	form
}) {
	const intro = sections.find((s) => s.slug === "join-partner-intro");
	//console.log(intro);
	const { contentHtml } = personae;

	const { title, subtitle, offer } = personae.frontmatter;
	//console.log(personae);

	return (
		<Layout siteMenu={siteMenu} footerMenu={footerMenu} socialMenu={socialMenu}>
			<Section>
				<SectionIntro blocks={intro.frontmatter.blocks} />
				<SectionJoinConcept title={subtitle} content={contentHtml} />
				<SectionJoinOffer offer={offer} />
				{/*<SectionJoinForm /> */}
				<Form form = {form}></Form>
			</Section>
		</Layout>
	);
}

export const getStaticProps = async (props) => {
	const siteMenu = await getMenu("site", props);
	const footerMenu = await getMenu("footer", props);
	const socialMenu = await getMenu("social", props);

	const sections = await getPageSections("join-partner", props);
	const personae = await getPersonae("partner", props);
    const form = await getForm("join-partner",props);

	return {
		props: {
			siteMenu,
			footerMenu,
			socialMenu,
			personae,
			sections,
			form
		},
	};
};
