import pageStyles from '@/styles/pages/join.module.css'
import Layout from '@/components/layout'
import Section from '@/components/section'
import SectionIntro from '@/components/sections/section-intro'
import SectionJoinConcept from '@/components/sections/join-concept'
import Course from '@/components/courses/course'
import Form from '@/components/forms/form'
import slug from '@/public/admin/fields/slug'
import {
	getMenu,
	getPageSections,
	getPersonae,
	getCourse,
	getForm,
} from '@/utils/api.js'

export default function InternationalTraineePage({
	siteMenu,
	footerMenu,
	socialMenu,
	sections,
	personae,
	course,
	form
}) {
	const intro = sections.find(s => s.slug === 'join-international-trainee-intro')
	const {contentHtml} = personae
	const {
		title,
		subtitle,
	} = personae.frontmatter

	return (
		<Layout
			siteMenu={siteMenu}
			footerMenu={footerMenu}
			socialMenu={socialMenu}
		>
			<Section>
				<SectionIntro blocks={intro.frontmatter.blocks}/>
				<SectionJoinConcept title={subtitle} content={contentHtml}/>
				<Course course={course}/>
				<Form form = {form}></Form>
			</Section>
		</Layout>
	)
}

export const getStaticProps = async (props) => {

	const siteMenu = await getMenu('site', props)
	const footerMenu = await getMenu('footer', props)
	const socialMenu = await getMenu('social', props)

	const sections = await getPageSections('join-international-trainee', props)
	const personae = await getPersonae('international-trainee', props)
	const course = await getCourse('international-trainee', props)
	const form = await getForm('international-trainee',props)
	return {
		props: {
			siteMenu,
			footerMenu,
			socialMenu,
			sections,
			personae,
			course,
			form
		}
	}
}
