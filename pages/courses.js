import pageStyles from '@/styles/pages/courses.module.css'
import Layout from '@/components/layout'
import Section from '@/components/section'
import List from '@/components/courses/list'

import {
	getMenu,
	getCourses,
	getPage,
	getPageSections
} from '@/utils/api.js'

export default function CoursesPage({
	siteMenu,
	footerMenu,
	socialMenu,
	courses,
	page,
	sections
}) {
	return (
		<Layout
			siteMenu={siteMenu}
			footerMenu={footerMenu}
			socialMenu={socialMenu}
		>
			<Section>
				<h1 className={pageStyles.title}>{page.frontmatter.title}</h1>
				{courses.length && <List courses={courses}></List>}
			</Section>
		</Layout>
	)
}

export const getStaticProps = async (props) => {
	const siteMenu = await getMenu('site', props)
	const footerMenu = await getMenu('footer', props)
	const socialMenu = await getMenu('social', props)

	const courses = await getCourses(props)
	const page = await getPage('courses', props)
	const sections = await getPageSections('courses', props)
	return {
		props: {
			siteMenu,
			footerMenu,
			socialMenu,
			courses,
			page,
			sections,
		}
	}
}
