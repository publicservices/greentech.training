import Layout from '@/components/layout'
import Section from '@/components/section'
import Form from '@/components/forms/form'

import styles from '@/styles/pages/index.module.css'

import {
	getMenu,
	getPage,
	getForm,
} from '@/utils/api'

export default function Page({
	siteMenu,
	footerMenu,
	socialMenu,
	page,
	form
}) {
	return (
		<Layout
			siteMenu={siteMenu}
			footerMenu={footerMenu}
			socialMenu={socialMenu}
		>
			<Section fullHeight={true}>
				<article className={
styles.page
}>
					<header className={styles.header}>
						<h1 className={styles.title}>{page.frontmatter.title}</h1>
					</header>
					<main className={styles.content}>
						{page.content}
					</main>
				</article>
				<Form form = {form}></Form>
			</Section>
		</Layout>
	)
}

export const getStaticProps = async (props) => {
	const siteMenu = await getMenu('site', props)
	const footerMenu = await getMenu('footer', props)
	const socialMenu = await getMenu('social', props)
    const form = await getForm('contact',props)
	const page = await getPage('contact', props)
	return {
		props: {
			siteMenu,
			footerMenu,
			socialMenu,
			page,
			form
		}
	}
}
