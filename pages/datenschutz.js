import Layout from '@/components/layout'
import Section from '@/components/section'

import styles from '@/styles/pages/default.module.css'

import {
	getMenu,
	getPage,
} from '@/utils/api'

export default function Page({
	siteMenu,
	footerMenu,
	socialMenu,
	page,
}) {
	return (
		<Layout
			siteMenu={siteMenu}
			footerMenu={footerMenu}
			socialMenu={socialMenu}
		>
			<Section>
				<article className={styles.page}>
					<header className={styles.header}>
						<h1 className={styles.title}>{page.frontmatter.title}</h1>
					</header>
					<main className={styles.content}>
						<div dangerouslySetInnerHTML={{ __html: page.contentHtml }}></div>
					</main>
				</article>
			</Section>
		</Layout>
	)
}

export const getStaticProps = async (props) => {
	const siteMenu = await getMenu('site', props)
	const footerMenu = await getMenu('footer', props)
	const socialMenu = await getMenu('social', props)

	const page = await getPage('datenschutz', props)
	return {
		props: {
			siteMenu,
			footerMenu,
			socialMenu,
			page,
		}
	}
}
