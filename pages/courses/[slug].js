import Layout from '@/components/layout'
import Section from '@/components/section'
import SectionIntro from '@/components/sections/section-intro'
import SectionConcept from '@/components/sections/course-concept'
import Course from '@/components/courses/course'
import pageStyles from '@/styles/pages/course.module.css'
import Form from '@/components/forms/form'
import {
	getMenu,
	getCourses,
	getCourse,
	getSection,
	getForm,
} from '@/utils/api.js'

export default function CoursePage({
	course,
	form,
	section,
	siteMenu,
	footerMenu,
	socialMenu,
}) {
	return (
		<Layout siteMenu={siteMenu} footerMenu={footerMenu} socialMenu={socialMenu}>
			{section ? (
				<Section fullHeight>
					<SectionIntro blocks={section.frontmatter.blocks}/>
				</Section>
			) : null}

			<Section>
				<SectionConcept
				title={course.frontmatter.subtitle}
				content={course.contentHtml}
				/>
			</Section>

			<Section fullHeight>
				<Course course={course}/>
			</Section>
			<Section>
			<Form form={form}></Form>
			</Section>
		</Layout>
	)
}

export async function getStaticProps(props) {
	const {slug} = props.params
	const siteMenu = await getMenu('site', props)
	const footerMenu = await getMenu('footer', props)
	const socialMenu = await getMenu('social', props)

	const course = await getCourse(slug, props)
	const section = await getSection(`course-${slug}-intro`, props)
	const form = await getForm(slug,props)
	return {
		props: {
			siteMenu,
			footerMenu,
			socialMenu,
			course,
			section,
			form
		},
	}
}

export async function getStaticPaths(props) {
	const courses = await getCourses(props)
	const coursesSlugs = courses.map(course => {
		return {
			params: {
				slug: course.slug
			}
		}
	})
	return {
		paths: coursesSlugs,
		fallback: false
	}
}
