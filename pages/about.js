import pageStyles from '@/styles/pages/about.module.css'
import layoutStyles from '@/styles/components/layout.module.css'

import Link from 'next/link'
import Layout from '@/components/layout'
import Section from '@/components/section'

import SectionIntro from '@/components/sections/section-intro'
import SectionConcept from '@/components/sections/section-concept'
import SectionCTA from '@/components/sections/section-cta'

import {
	getMenu,
	getPage,
	getPageSections,
} from '@/utils/api'

export default function About({
	siteMenu,
	footerMenu,
	socialMenu,
	page,
	sections,
}) {
	const intro = sections.find(s => s.slug === 'about-us-intro')
	const concept = sections.find(s => s.slug === 'about-us-concept')
	const partner = sections.find(s => s.slug === 'about-us-partner')
	return (
		<Layout
			siteMenu={siteMenu}
			footerMenu={footerMenu}
			socialMenu={socialMenu}
		>
			{intro ? (
				<Section fullHeight transparent active>
					<SectionIntro blocks={intro.frontmatter.blocks}/>
				</Section>
			) : null}
			{concept ? (
				<Section fullHeight>
					<SectionConcept blocks={concept.frontmatter.blocks}/>
				</Section>
			) : null}
			{partner ? (
				<Section fullHeight bgTheme>
					<SectionCTA blocks={partner.frontmatter.blocks}/>
				</Section>
			) : null}
		</Layout>
	)
}

export const getStaticProps = async (props) => {
	const siteMenu = await getMenu('site', props)
	const footerMenu = await getMenu('footer', props)
	const socialMenu = await getMenu('social', props)

	const sections = await getPageSections('about', props)
	const page = await getPage('about', props)
	return {
		props: {
			siteMenu,
			footerMenu,
			socialMenu,
			page,
			sections,
		}
	}
}
