import Layout from "@/components/layout";
import Section from "@/components/section";
import SectionIntro from "@/components/sections/section-intro";

import styles from "@/styles/pages/default.module.css";

import { getMenu, getPage, getPageSections } from "@/utils/api";

export default function Page({
	siteMenu,
	footerMenu,
	socialMenu,
	page,
	sections,
}) {
	const intro = sections.find((s) => s.slug === "impressum-intro");
	return (
		<Layout siteMenu={siteMenu} footerMenu={footerMenu} socialMenu={socialMenu}>
			<Section>
				{intro ? <SectionIntro blocks={intro.frontmatter.blocks} /> : null}
				<main className={styles.container}>
					<div dangerouslySetInnerHTML={{ __html: page.contentHtml }}></div>
				</main>
			</Section>
		</Layout>
	);
}

export const getStaticProps = async (props) => {
	const siteMenu = await getMenu("site", props);
	const footerMenu = await getMenu("footer", props);
	const socialMenu = await getMenu("social", props);

	const page = await getPage("impressum", props);
	const sections = await getPageSections("impressum", props);
	return {
		props: {
			siteMenu,
			footerMenu,
			socialMenu,
			page,
			sections,
		},
	};
};
