import Layout from "@/components/layout";
import Section from "@/components/section";
import Intro from "@/components/sections/homepage-intro";
import SectionConcept from "@/components/sections/section-concept";
import ListCourses from "@/components/courses/list";
import SliderServices from "@/components/services/slider";

import pageStyles from "@/styles/pages/index.module.css";

import {
	getMenu,
	getPage,
	getPageSections,
	getCourses,
	getServices,
} from "@/utils/api";

const CoursesDisclaimer = ({ blocks }) => {
	const data = blocks[0];
	return (
		<aside className={pageStyles.disclaimerCourses}>
			<center>
				<sup>*</sup>
				{data.content}
			</center>
		</aside>
	);
};

export default function Home({
	siteMenu,
	footerMenu,
	socialMenu,
	page,
	pageServices,
	pageCourses,
	sections,
	courses,
	services,
}) {
	const intro = sections.find((s) => s.slug === "homepage-intro");
	const concept = sections.find((s) => s.slug === "homepage-concept");
	const coursesDisclaimer = sections.find(
		(s) => s.slug === "homepage-courses-disclaimer"
	);

	//console.log(coursesDisclaimer)

	return (
		<Layout siteMenu={siteMenu} footerMenu={footerMenu} socialMenu={socialMenu}>
			{intro ? (
				<Section fullHeight>
					<Intro blocks={intro.frontmatter.blocks} />
				</Section>
			) : null}
			{concept ? (
				<Section padding>
					<SectionConcept blocks={concept.frontmatter.blocks} />
				</Section>
			) : null}
			{services.length ? (
				<Section fullHeight bgLight>
					<SliderServices
						services={services}
						defaultTitle={pageServices.frontmatter.title}
						defaultContent={pageServices.content}
					/>
				</Section>
			) : null}
			{courses.length ? (
				<Section
					fullHeight
					title={pageCourses.frontmatter.title}
					padding
					bgHighlight
					theme
					active
					radius
				>
					<ListCourses courses={courses} />
					<CoursesDisclaimer blocks={coursesDisclaimer.frontmatter.blocks} />
				</Section>
			) : null}
		</Layout>
	);
}

export const getStaticProps = async (props) => {
	const siteMenu = await getMenu("site", props);
	const footerMenu = await getMenu("footer", props);
	const socialMenu = await getMenu("social", props);

	const page = await getPage("home", props);
	const pageCourses = await getPage("courses", props);
	const pageServices = await getPage("services", props);

	const sections = await getPageSections("home", props);

	const courses = await getCourses(props);
	const services = await getServices(props);
	return {
		props: {
			siteMenu,
			footerMenu,
			socialMenu,
			page,
			pageServices,
			pageCourses,
			sections,
			courses: courses.filter((c) => c.frontmatter.is_featured === true),
			services,
		},
	};
};
