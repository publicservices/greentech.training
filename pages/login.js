import pageStyles from '@/styles/pages/login.module.css'

import Link from 'next/link'
import Layout from '@/components/layout'
import Section from '@/components/section'

import {
	getMenu,
	getPage,
} from '@/utils/api'

export default function Login({
	siteMenu,
	footerMenu,
	socialMenu,
	page,
}) {
	const {frontmatter} = page
	const {title} = frontmatter
	return (
		<Layout
			siteMenu={siteMenu}
			footerMenu={footerMenu}
			socialMenu={socialMenu}
		>
			<Section fullHeight>
				<article className={pageStyles.root}>
					{title && (
						<header className={pageStyles.header}>
							<h1 className={pageStyles.title}>
								{title}
							</h1>
						</header>
					)}
					<main className={pageStyles.main}>
						<form
							className={pageStyles.form}
							onSubmit={(event) => {
								event.preventDefault()
								alert('Soon.')
							}}
						>
							<input placeholder="email" type="email"/>
							<input placeholder="password" type="password"/>
							<button type="submit">Login</button>
						</form>
					</main>
				</article>
			</Section>
		</Layout>
	)
}

export const getStaticProps = async (props) => {
	const siteMenu = await getMenu('site', props)
	const footerMenu = await getMenu('footer', props)
	const socialMenu = await getMenu('social', props)
	const page = await getPage('login', props)
	return {
		props: {
			siteMenu,
			footerMenu,
			socialMenu,
			page,
		}
	}
}
