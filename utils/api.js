import { promises as fs } from 'fs'
import path from 'path'
import * as matter from 'gray-matter'
import {unified} from 'unified'
import remarkParse from 'remark-parse'
import remarkRehype from 'remark-rehype'
import rehypeRaw from 'rehype-raw'
import rehypeStringify from 'rehype-stringify'
import rehypeSanitize, {defaultSchema} from 'rehype-sanitize'

/* because `next export` to static static,
	 does not yet support i18n routing, let's use the default locale
 */
const i18n = {
	locales: ['de'], // we don't use 'en' yet in the frontend
	defaultLocale: 'de',
}

/* a function to convert content in plain-text/markdown, to HTML */
const toHTML = async (contentMd) => {
	const file = await unified()
		.use(remarkParse)
		.use(remarkRehype, {allowDangerousHtml: true})
		.use(rehypeRaw)
		.use(rehypeStringify)
		.use(rehypeSanitize)
		.process(contentMd)
	return String(file)
}

/*
	 Private API
	 These functions are used to read content folders, for each models
 */

/* generic functions to read markdown files from a folder */
const getModelFolder = async (modelFolder, {
	locale = i18n.defaultLocale
}) => {
	const modelFolderPath = path.join(
		process.cwd(),
		'content',
		modelFolder,
		locale,
	)

	const itemsToRead = await fs.readdir(modelFolderPath)

	const modelItems = itemsToRead.map(modelItemFileName => {
		return getModelItem(modelFolder, modelItemFileName, {locale})
	})

	const items = await Promise.all(modelItems)
	return items || []
}

const getModelItem = async (modelFolder, modelItemFileName, {locale}) => {
	const modelFilePath = path.join(
		process.cwd(),
		'content',
		modelFolder,
		locale,
		modelItemFileName
	)

	let modelFileContent
	try {
		modelFileContent = await fs.readFile(modelFilePath, 'utf8')
	} catch (error) {
		console.error(`Missing file:`, modelFilePath)
	}

	let model,
			frontmatter;
	if (modelFileContent) {
		frontmatter = matter(modelFileContent)
		model = {
			slug: frontmatter.data.slug || modelItemFileName.split('.md')[0],
			frontmatter: frontmatter.data,
			content: frontmatter.content,
			contentHtml: await toHTML(frontmatter.content),
		}
	}
	return model
}

/*
	 public api
	 these methods, should be used to fetch contents
 */

/* MODEL: menus */
const getMenus = async ({locale}) => {
	return getModelFolder('menus', {locale})
}
const getMenu = async (name, {locale}) => {
	const menus = await getMenus({locale})
	const menu = menus.find(item => {
		return item.slug === name
	})
	return menu
}

/* MODEL: pages */
const getPages = async ({locale}) => {
	return getModelFolder('pages', {locale})
}

const getPage = async (slug, {locale}) => {
	const pages = await getPages({locale})
	const page = pages.find(item => {
		return item.slug === slug
	})
	return page
}

const getPageSections = async (slug, {locale}) => {
	const page = await getPage(slug, {locale})
	const sectionSlugs = page.frontmatter?.sections || []
	const promises = sectionSlugs.map((sectionSlug => {
		return getSection(sectionSlug, {locale})
	}))
	return Promise.all(promises)
}

/* MODEL: sections */
const getSections = async ({locale}) => {
	const sections = getModelFolder('sections', {locale})
	if (sections && sections.length) {
		return Promise.resolve(sections.map(serializeSection))
	}
	return sections
}

const getSection = async (slug, {locale}) => {
	const sections = await getSections({locale})
	const section = sections.find(item => {
		return item.slug === slug
	})
	return section || null
}

/* MODEL: courses */
const getCourses = async ({locale}) => {
	return getModelFolder('courses', {locale})
}

const getCourse = async (slug, {locale}) => {
	const courses = await getCourses({locale})
	const course = courses.find(item => {
		return item.slug === slug
	})
	return course
}

/* MODEL: services */
const getServices = async ({locale}) => {
	const sortServices = (a,b) => {
		return a.frontmatter.index - b.frontmatter.index
	}
	const services = await getModelFolder('services', {locale})
	return services.sort(sortServices)
}

const getService = async (slug, {locale}) => {
	const services = await getServices({locale})
	const service = services.find(item => {
		return item.slug === slug
	})
	return service
}

/* MODEL: personas */
const getPersonas = async ({locale}) => {
	const personas = await getModelFolder('personas', {locale})
	return personas
}

const getPersonae = async (slug, {locale}) => {
	const personas = await getPersonas({locale})
	const personae = personas.find(item => {
		return item.slug === slug
	})
	return personae
}

/* MODEL: faqs */
const getFaqs = async ({locale}) => {
	const faqs = await getModelFolder('faqs', {locale})
	return faqs
}

const getFaq = async (slug, {locale}) => {
	const faqs = await getFaqs({locale})
	const faq = faqs.find(item => {
		return item.slug === slug
	})
	return faq
}

/* serializers */
const serializeSection = async (section) => {
	if (section.frontmatter.blocks) {
		const blocks = await Promise.all(section.frontmatter.blocks.map(serializeBlock))
		section.frontmatter.blocks = blocks
	}
	return section
}
const serializeBlock = async (block) => {
	if (block.content) {
		block.contentHtml = await toHTML(block.content)
	}
	return block
}

/* MODEL: courses */
const getForms = async ({locale}) => {
	return getModelFolder('forms', {locale})
}

const getForm = async (slug, {locale}) => {
	const forms = await getForms({locale})
	const form = forms.find(item => {
		return item.slug === slug
	})
	return form
}

export {
	i18n,
	getMenus,
	getMenu,
	getSections,
	getSection,
	getPages,
	getPage,
	getPageSections,
	getCourses,
	getServices,
	getPersonas,
	getPersonae,
	getCourse,
	getFaqs,
	getForm
}
