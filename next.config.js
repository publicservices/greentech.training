/* absolute imports are in `./jsconfig.json`
 * https://nextjs.org/docs/advanced-features/module-path-aliases */

/* next-js config */
const nextConfig = {
	reactStrictMode: true,
	swcMinify: true,

	/* the base path, where the app is served */
	basePath: "/projects/greentech.training",

	/* because we use `next export` */
	experimental: {
		images: {
			unoptimized: true,
			allowFutureImage: true,
		},
	},

	/* i18n not support with SSG `next export` */
	/* i18n setup */
	/* i18n: {
	 *   locales: ['de'],
	 *   defaultLocale: 'de',
	 * }, */
};

module.exports = nextConfig;
