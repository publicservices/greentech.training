export default {
	label: "Program",
	name: "program",
	widget: "list",
	fields: [
		{
			label: "Identifier",
			name: "identifier",
			widget: "string",
			required: true,
		},
		{
			label: "Title",
			name: "title",
			widget: "text",
			required: false,
		},
		{
			label: "Description",
			name: "body",
			widget: "text",
			required: true,
		},
	],
}
