export default {
	i18n: true,
	label: 'Duration',
	name: 'duration',
	widget: 'text',
	required: true,
	hint: 'A written text describing the duration (in time)'
}
