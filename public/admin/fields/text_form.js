export default {
	i18n: true,
	label: 'text_form',
	name: 'text_form',
	widget: 'text',
	required: true,
	hint: 'A written text describing the text form'
}