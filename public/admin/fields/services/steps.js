export default {
	i18n: true,
	label: 'Steps',
	name: 'steps',
	widget: 'list',
	hint: 'An ordered list of steps, in this service.',
	fields: [
		{
			label: 'Name',
			name: 'name',
			widget: 'text',
			required: true,
		}
	]
}
