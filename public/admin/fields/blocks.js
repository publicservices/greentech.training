/*
	 This content type uses netlify-cms beta feature:
	 https://www.netlifycms.org/docs/beta-features/#list-widget-variable-types
 */

const imageBlock = {
	label: "Image",
	name: "image",
	widget: "object",
	fields: [
		{
			label: "Title",
			name: "title",
			widget: "string",
			required: true,
		},
		{
			label: "Image Source",
			name: "src",
			widget: "image",
			required: true,
			media_library: { config: { multiple: false } },
		},
	],
}
const ctaBlock = {
	label: "CTA",
	name: "cta",
	widget: "object",
	fields: [
		{
			label: "Title",
			name: "title",
			widget: "string",
			required: true,
		},
		{
			label: "Link",
			name: "link",
			widget: "string",
		},
	],
}

const contentBlock = {
	label: "Content",
	name: "content",
	widget: "object",
	fields: [
		{
			name: "content",
			widget: "markdown",
			required: true,
		},
	],
}

const titleBlock = {
	label: "Title",
	name: "title",
	widget: "object",
	fields: [
		{
			name: "content",
			widget: "text",
			required: true,
		},
	],
}

const blockTypes = [
	titleBlock,
	imageBlock,
	ctaBlock,
	contentBlock,
]

export default {
	label: "Blocks",
	name: "blocks",
	widget: "list",
	types: blockTypes,
}
