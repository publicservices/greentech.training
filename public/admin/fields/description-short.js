export default {
	i18n: true,
	label: 'Short Description',
	name: 'description_short',
	widget: 'markdown',
	required: false,
	hint: 'Write a short text description to present this item'
}
