export default {
	label: 'Index',
	name: 'index',
	widget: 'number',
	required: false,
	default: 1,
	min: 1,
	step: 1,
	max: 99,
	hint: 'The index is the position (1 is earliest in list) of an element in a list',
}
