export default {
	i18n: true,
	label: 'Title',
	name: 'title',
	widget: 'string',
	required: true,
	hint: 'The Title is the main short text for this item. It is used accross the website when listing this item.'
}
