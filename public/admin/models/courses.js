import title from "../fields/title.js";
import slug from "../fields/slug.js";
import body from "../fields/body.js";
import duration from "../fields/duration.js";
import descriptionShort from "../fields/description-short.js";
import program from "../fields/program.js";

const courses = {
	i18n: true,
	format: "yaml-frontmatter",
	extension: "md",
	name: "courses",
	label: "Courses",
	label_singular: "Course",
	folder: "content/courses/de",
	create: true,
	slug: "{{title}}",
	fields: [
		title,
		slug,
		descriptionShort,
		{
			label: "src",
			name: "src",
			widget: "string",
		},
		{
			i18n: true,
			label: "Subtitle",
			name: "subtitle",
			widget: "string",
			hint: "The Subtitle of the next to the main content",
		},
		body,
		duration,
		program,
		{
			label: "Is featured",
			name: "is_featured",
			widget: "boolean",
			hint: "Is this element a featured content (can appear on special pages)",
			default: false,
			required: false,
		},
	],
};

export default courses;
