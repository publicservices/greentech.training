import title from '../fields/title.js'
import slug from '../fields/slug.js'
import blocks from '../fields/blocks.js'

const pages = {
	i18n: true,
	format: 'yaml-frontmatter',
	extension: 'md',
	name: 'sections',
	label: 'Sections',
	label_singular: 'Section',
	create: false,
	delete: false,
	folder: 'content/sections/de',
	fields: [
		slug,
		blocks,
	],
	editor: {
		preview: false,
	},
}

export default pages
