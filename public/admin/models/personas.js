import title from '../fields/title.js'
import slug from '../fields/slug.js'
import body from '../fields/body.js'

const personas = {
	i18n: true,
	format: 'yaml-frontmatter',
	extension: 'md',
	name: 'personas',
	label: 'Personas',
	label_singular: 'Personae',
	create: false,
	delete: false,
	folder: 'content/personas/de',
	fields: [
		title,
		slug,
		{
			label: "Subtitle",
			name: "subtitle",
			widget: "string",
			required: true,
		},
		body,
		{
			label: 'Offer',
			name: 'offer',
			widget: 'object',
			required: false,
			fields: [
				{
					label: "Title",
					name: "title",
					widget: "string",
					required: false,
				},
				{
					label: "Blocks",
					name: "blocks",
					widget: "list",
					fields: [
						{
							label: "Title",
							name: "title",
							widget: "string",
							required: true,
						},
						{
							label: "Description",
							name: "body",
							widget: "string",
							required: true,
						},
					]
				},
			]
		}
	],
}

export default personas
