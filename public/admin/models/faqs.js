import title from '../fields/title.js'
import slug from '../fields/slug.js'

const faq = {
	i18n: true,
	format: 'yaml-frontmatter',
	extension: 'md',
	name: 'faq',
	label: 'FAQs',
	label_singular: 'FAQ',
	create: true,
	folder: 'content/faqs/de',
	fields: [
		title,
		slug,
		{
			name: "questions",
			label: "Questions",
			label_singular: "Question",
			i18n: true,
			widget: "list",
			fields: [
				{
					label: "Title",
					name: "title",
					widget: "string",
					required: true,
				},
				{
					label: "Description",
					name: "body",
					widget: "text",
					required: true,
				},
			]
		}
	],
}

export default faq
