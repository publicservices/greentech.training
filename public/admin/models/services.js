import title from '../fields/title.js'
import slug from '../fields/slug.js'
import index from '../fields/index.js'
import body from '../fields/body.js'
import steps from '../fields/services/steps.js'


const courses	= {
	i18n: true,
	format: 'yaml-frontmatter',
	extension: 'md',
	name: 'services',
	label: 'Services',
	label_singular: 'Service',
	folder: 'content/services/de',
	create: false,
	delete: false,
	slug: '{{title}}',
	fields: [
		title,
		slug,
		index,
		body,
		steps,
	]
}

export default courses
