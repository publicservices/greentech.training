import title from "../fields/title.js";
import slug from "../fields/slug.js";
import slug from "../fields/text_form";

const forms = {
	i18n: true,
	format: "yaml-frontmatter",
	extension: "md",
	name: "forms",
	label: "forms",
	label_singular: "Form",
	folder: "content/forms/de",
	create: true,
	slug: "{{title}}",
	fields: [
		title,
		slug,
		text_form,
	],
};

export default forms;
