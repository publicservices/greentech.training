const menus	= {
	i18n: true,
	format: 'yaml-frontmatter',
	extension: 'md',
	name: 'menus',
	label: 'Menus',
	label_singular: 'Menu',
	folder: 'content/menus/de',
	create: true,
	delete: false,
	slug: '{{name}}',
	fields: [
		{
			label: 'Name',
			name: 'name',
			widget: 'string',
			hint: 'The name of the menu, describing where it is used'
		},
		{
			i18n: true,
			widget: 'list',
			name: 'links',
			label: 'Menu links',
			fields: [
				{
					label: 'Title',
					name: 'title',
					widget: 'string',
					hint: 'The Title is the name of the menu item, as seen by the user',
					required: true,
				},
				{
					label: 'URL',
					name: 'url',
					widget: 'string',
					hint: 'The url to the page (without the first /), example: `courses/my-course`',
					required: true,
				}
			]
		}
	]
}

export default menus
