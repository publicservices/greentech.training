import title from '../fields/title.js'
import slug from '../fields/slug.js'
import body from '../fields/body.js'

const pages = {
	i18n: true,
	format: 'yaml-frontmatter',
	extension: 'md',
	name: 'pages',
	label: 'Pages',
	label_singular: 'Page',
	create: false,
	delete: false,
	folder: 'content/pages/de',
	fields: [
		title,
		slug,
		body,
		{
			label: 'Sections',
			name: 'sections',
			widget: 'relation',
			collection: 'sections',
			search_fields: ['slug'],
			value_field: 'slug',
			display_fields: ['slug'],
			multiple: true,
			required: false,
		}
	],
}

export default pages
