import menus from "./models/menus.js";
import pages from "./models/pages.js";
import courses from "./models/courses.js";
import sections from "./models/sections.js";
import services from "./models/services.js";
import personas from "./models/personas.js";
import faqs from "./models/faqs.js";

export default {
	config: {
		/* Skips config.yml.
			 By not skipping, the configs will be merged, with the js-version taking priority. */
		load_config_file: false,
		display_url: window.location.origin,

		/* only use when testing locally in dev mode, and makes changes to local files; `npx-netlify-cms-proxy-server` */
		local_backend: true,
		/* when site admin is live, and commits to gitlab */
		backend: {
			name: "gitlab",
			branch: "main",
			repo: "publicservices/projects/greentech.training",
			auth_type: "pkce",
			app_id:
				"4ef9fb0742ca00c578b4aaca719428181ba30d5f545692384365ad51e266a86c",
		},

		media_folder: "public/media/images",
		public_folder: "media/images",

		/* configuration for multilanguage
			 expects to persists files in `<folder>/<locale>/<slug>.<extension>
			 Uncomment i18n object, and remove the `/de` trailing path
			 of models configurations, when want to activate i18n;
			 the nextjs app system/content is already supporting it
		 */
		/* i18n: {
			 structure: 'multiple_folders',
			 locales: ['de', 'en'],
			 default_locale: 'de',
			 }, */

		/* our data models */
		collections: [pages, sections, courses, services, personas, faqs, menus],
	},
};
